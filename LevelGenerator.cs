using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class LevelGenerator : MonoBehaviour
{
    [SerializeField] private float width = 16;
    [SerializeField] private float height = 16;
    [SerializeField] private GameObject cube;
    [SerializeField] private float perlinNoiseMultipler = 0.3f;
    [SerializeField] private float scale = 10;
    [SerializeField] private float depth = 10;
    [SerializeField] private Material[] materials;

    private float randomX = 0;
    private float randomZ = 0;

    // Start is called before the first frame update
    void Start()
    {
        GenerateTerrain();
    }

    private void GenerateTerrain()
    {
        randomX = Random.Range(0, 700);
        randomZ = Random.Range(0, 700);

        for(int x = 0; x < width; x++)
        {
            for(int z = 0; z < height; z++)
            {
                //float y = Mathf.PerlinNoise(x*perlinNoiseMultipler, z*perlinNoiseMultipler)*scale;
                float y = CalulatePerlinNoise(x, z)*depth;
                int yInt = (int)y;
                Vector3 pos = new Vector3(x, yInt, z);
                GameObject cubeClone = Instantiate(cube, pos, Quaternion.identity);
                MeshRenderer renderer = cubeClone.GetComponent<MeshRenderer>();
                Material materialCube = null;
                if(yInt >= 0 && yInt <= 2)
                {
                    materialCube = materials[0];
                }else if(yInt >= 3 && yInt <= 4)
                {
                    materialCube = materials[1];
                }else if(yInt >= 5 && yInt <= 7)
                {
                    materialCube = materials[2];
                }
                else
                {
                    materialCube = materials[3];
                }


                renderer.material = materialCube;
            }
        }
    }

    private float CalulatePerlinNoise(int x,int z)
    {
        float xCoord = (float)x / width * scale + randomX;
        float zCoord = (float)z / height * scale + randomZ;

        return Mathf.PerlinNoise(xCoord, zCoord);
    }
}
