using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.InputSystem;
using UnityEngine.InputSystem.Utilities;
using UnityEngine.UI;

public class SelectControllers : MonoBehaviour
{
    [SerializeField] private PlayerInput playerInput1;
    [SerializeField] private PlayerInput playerInput2;

    [SerializeField] private Dropdown dropdown1;
    [SerializeField] private Dropdown dropdown2;


    // Start is called before the first frame update
    void Start()
    {
        AddControllers(dropdown1);
        AddControllers(dropdown2);

        OnValueChangeDropdawn1();
        OnValueChangeDropdawn2();
    }

    // Update is called once per frame
    void Update()
    {
        
    }

    public void OnValueChangeDropdawn1()
    {
        OnValueChangedInput(dropdown1, playerInput1);
    }

    public void OnValueChangeDropdawn2()
    {
        OnValueChangedInput(dropdown2, playerInput2);
    }

    private void OnValueChangedInput(Dropdown dropdown, PlayerInput playerInput)
    {
        Dropdown.OptionData optionDataSelected = dropdown.options[dropdown.value];

        bool isGamepadSeleted = false;
        int idGamepad = 0;
        foreach(Gamepad gamepad in Gamepad.all)
        {
            if (gamepad.displayName.Equals(optionDataSelected.text))
            {
                isGamepadSeleted = true;

                playerInput.SwitchCurrentControlScheme("Gamepad", Gamepad.all[idGamepad]);
                break;
            }
            idGamepad++;
        }

        if (!isGamepadSeleted)
        {
            playerInput.SwitchCurrentControlScheme("Keyboard&Mouse", Keyboard.current,Mouse.current);
        }

        /*if (dropdown.options.IndexOf(optionDataSelected) != -1)
        {
            dropdown.options.RemoveAt(dropdown.options.IndexOf(optionDataSelected));
        }*/
    }

    private void AddControllers(Dropdown dropdown)
    {
        ReadOnlyArray<Gamepad> gamepads = Gamepad.all;
        List<Dropdown.OptionData> optionDataList = new List<Dropdown.OptionData>();

        foreach(Gamepad gamepad in gamepads)
        {
            Dropdown.OptionData optionData = new Dropdown.OptionData(gamepad.displayName);
            optionDataList.Add(optionData);
        }

        if(Keyboard.current != null && Mouse.current != null)
        {
            Dropdown.OptionData optionDataKeyBoardMouse = new Dropdown.OptionData(Keyboard.current.displayName);
            optionDataList.Add(optionDataKeyBoardMouse);
        }

        dropdown.options = optionDataList;
    }
}
