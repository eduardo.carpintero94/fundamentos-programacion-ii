using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class GameManager : MonoBehaviour
{
    public static GameManager instance = null;

    public GameObject player1;
    public GameObject player2;

    [SerializeField] private LayerMask layerPlayer1;
    [SerializeField] private LayerMask layerPlayer2;

    // Start is called before the first frame update
    void Awake()
    {
        if(instance == null)
        {
            instance = this;
        }
        ChangePlayerLayer(player1, "Player1", layerPlayer2);
        ChangePlayerLayer(player2, "Player2", layerPlayer1);
    }

    private void ChangePlayerLayer(GameObject player,string playerLayerMask ,LayerMask otherLayerMask)
    {
        player.layer = LayerMask.NameToLayer(playerLayerMask);

        foreach(Transform trPlayerChild in player.GetComponentsInChildren<Transform>())
        {
            trPlayerChild.gameObject.layer = LayerMask.NameToLayer(playerLayerMask);
        }

        CheckDamage[] playerCheckDamages = player.GetComponentsInChildren<CheckDamage>();
        foreach (CheckDamage playerCheckDamage in playerCheckDamages)
        {
            playerCheckDamage.SetLayerMask(otherLayerMask);
        }
    }

    // Update is called once per frame
    void Update()
    {
        
    }
}
