using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.InputSystem;

public class Player : MonoBehaviour
{
    private Animator animator;
    private float horizontal;

    [SerializeField] private CheckDamage leftPunch;
    [SerializeField] private CheckDamage rightPunch;
    [SerializeField] private CheckDamage leftKick;
    [SerializeField] private CheckDamage rightKick;

    private int life = 100;
    [SerializeField] private Image hearthBar;

    private Rigidbody[] rbChild;

    private void Start()
    {
        animator = GetComponent<Animator>();
        rbChild = GetComponentsInChildren<Rigidbody>();
    }

    private void FixedUpdate()
    {
        animator.SetFloat("Movement", horizontal);
        foreach(Rigidbody rb in rbChild)
        {
            rb.position = rb.transform.position;
        }
    }

    private void OnLeftPunch(InputValue value)
    {
        ExecuteAttack(value.isPressed, 1);
    }

    private void OnRightPunch(InputValue value)
    {
        ExecuteAttack(value.isPressed, 2);
    }

    private void OnLeftKick(InputValue value)
    {
        ExecuteAttack(value.isPressed, 3);
    }

    private void OnRightKick(InputValue value)
    {
        ExecuteAttack(value.isPressed, 4);
    }

    private void ExecuteAttack(bool isPressed, int numAttack)
    {
        if (isPressed)
        {
            animator.SetInteger("attacks", numAttack);
            CheckCanDamage(numAttack,true);
        }
    }

    private void CheckCanDamage(int numAttack, bool canDamage)
    {
        switch (numAttack)
        {
            case 1:
                leftPunch.canCheck = canDamage;
                break;
            case 2:
                rightPunch.canCheck = canDamage;
                break;
            case 3:
                leftKick.canCheck = canDamage;
                break;
            case 4:
                rightKick.canCheck = canDamage;
                break;
        }
    }

    public void DisableCanDamage(int numAttack)
    {
        CheckCanDamage(numAttack, false);
    }

    private void OnMove(InputValue value)
    {
        horizontal = value.Get<Vector2>().x;
    }

    private void OnFire(InputValue value)
    {
        if (value.isPressed)
        {
            Debug.Log("DISPARO "+gameObject.name);
        }
    }

    public void UpdateLife(int damage)
    {
        life -= damage;
        hearthBar.fillAmount = (float)life / 100;
        if (life <= 0)
        {
            GetComponent<Animator>().enabled = false;
        }
    }
}
