using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CheckDamage : MonoBehaviour
{
    [SerializeField] private float radiusSphere = 2;
    [SerializeField] private Vector3 offset = Vector3.zero;
    [SerializeField] private LayerMask layerMask;

    public bool canCheck = false;

    public Player otherPlayer;

    // Start is called before the first frame update
    void Start()
    {
        string nameLayer = LayerMask.LayerToName(gameObject.layer);

        if (nameLayer.Equals("Player1"))
        {
            otherPlayer = GameManager.instance.player2.GetComponent<Player>();
        }
        else
        {
            otherPlayer = GameManager.instance.player1.GetComponent<Player>();
        }
    }

    // Update is called once per frame
    void Update()
    {
        if (canCheck)
        {
            Collider[] collidersCollisions = Physics.OverlapSphere(transform.position + offset, radiusSphere, layerMask);
            if (collidersCollisions.Length > 0)
            {
                otherPlayer.UpdateLife(10);
                canCheck = false;
            }
        }
    }

    private void OnDrawGizmos()
    {
        Gizmos.color = Color.red;
        Gizmos.DrawSphere(transform.position+offset, radiusSphere);
    }

    public void SetLayerMask(LayerMask playerLayerMask)
    {
        layerMask = playerLayerMask;
    }
}
