using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[System.Serializable]
public class GameData
{
    private Player[] players;

    public GameData(Player[] players)
    {
        this.Players = players;
    }

    public int NumPlayers { get => Players.Length; }
    public Player[] Players { get => players; set => players = value; }

    public override string ToString()
    {
        string playersString = "{";
        foreach(Player player in players)
        {
            playersString += player.NamePlayer + "-" + player.Points + ",";
        }
        playersString += "}";
        return playersString;
    }
}
