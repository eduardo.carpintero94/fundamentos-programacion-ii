using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[System.Serializable]
public class Player
{
    private string namePlayer;
    private int points;

    public Player(string namePlayer, int points)
    {
        this.namePlayer = namePlayer;
        this.Points = points;
    }

    public string NamePlayer { get => namePlayer;}
    public int Points { get => points; set => points = value; }

    public override string ToString()
    {
        return NamePlayer + "-" + points;
    }
}
