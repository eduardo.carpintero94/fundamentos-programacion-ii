using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class RankingControll : MonoBehaviour
{
    private GameData gameData;

    [SerializeField] private InputField inputPlayerName;
    [SerializeField] private InputField inputPoints;
    [SerializeField] private Transform trRanking;
    [SerializeField] private Text textPlayer;
    [SerializeField] private Text txtNumPlayers;

    // Start is called before the first frame update
    void Start()
    {
        DataManager.Initialice();
        gameData = DataManager.gameData;

        ViewData();
    }

    public void Save()
    {
        string playerName = inputPlayerName.text;
        int points = int.Parse(inputPoints.text);

        gameData.Players = AddToList(gameData.Players,playerName,points);

        DataManager.WriteData(gameData);

        ViewData();
    }

    private void ViewData()
    {
        for(int i = 0; i < trRanking.childCount; i++)
        {
            if (trRanking.GetChild(i).gameObject.activeSelf)
            {
                Destroy(trRanking.GetChild(i).gameObject);
            }
        }
        foreach(Player player in gameData.Players)
        {
            Text textPlayerClone = Instantiate(textPlayer, Vector3.zero, Quaternion.identity, trRanking);
            textPlayerClone.text = player.ToString();
            textPlayerClone.gameObject.SetActive(true);
        }

        txtNumPlayers.text = "Num Players: " + gameData.NumPlayers;
    }


    private Player[] AddToList(Player[] playerArray, string playerName, int points)
    {
        List<Player> playerList = new List<Player>();
        playerList.AddRange(playerArray);

        int indexNamePlayer = NameIndexOf(playerList, playerName);
        if ( indexNamePlayer != -1){
            playerList[indexNamePlayer].Points = points;
        }
        else
        {
            playerList.Add(new Player(playerName, points));
        }
        
        playerList.Sort((Player p1, Player p2) => p1.Points.CompareTo(p2.Points));
        playerList.Reverse();

        return playerList.ToArray();
    }

    private int NameIndexOf(List<Player> playerList,string namePlayer)
    {
        int i = 0;
        foreach(Player player in playerList)
        {
            if (player.NamePlayer.Equals(namePlayer))
            {
                return i;
            }
            i++;
        }
        return -1;
    }
}
