using System;
using System.Collections;
using System.Collections.Generic;
using System.IO;
using System.Runtime.Serialization.Formatters.Binary;
using UnityEngine;

public class DataManager
{
    private static string path = Application.persistentDataPath + "/gameData.dat";
    private static FileStream fileStream;
    private static BinaryFormatter bf = new BinaryFormatter();
    
    public static GameData gameData;

    public static void Initialice()
    {
        if (File.Exists(path))
        {
            ReadData();
            Debug.Log("SE HAN LEIDO LOS DATOS....");
        }
        else
        {
            gameData = new GameData(new Player[0]);
            WriteData(gameData);
            Debug.Log("SE CREARON DATOS POR DEFECTO....");
        }
    }

    public static void WriteData(GameData myGameData)
    {
        fileStream = new FileStream(path, FileMode.Create);
        bf.Serialize(fileStream, myGameData);

        fileStream.Close();
    }

    public static void ReadData()
    {
        fileStream = new FileStream(path, FileMode.Open);
        gameData = (GameData)bf.Deserialize(fileStream);

        fileStream.Close();
    }
}
