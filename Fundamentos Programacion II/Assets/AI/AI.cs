using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.AI;

public class AI : MonoBehaviour
{
    //Estados de la IA
    private enum States {Route,FollowPlayer,Attack}

    //ScripteableObject
    [SerializeField] private AIObject aIObject;

    //NavMeshAgent con lo que movemeremos la IA
    private NavMeshAgent agent;
    //Lista de rutas para el path finding
    private List<Vector3> paths = new List<Vector3>();
    //Posicion de destino
    private Vector3 destinationPosition = Vector3.zero;
    //Id de la posicion del Array de paths
    private int idPos = 0;
    //Corrutina para que la IA espere en el punto de destino de la ruta
    private Coroutine coroutineGoToRoute = null;
    //Variables con los estados de la ruta
    [SerializeField]private States states = States.Route;
    //Transform del jugador
    private Transform trPlayer;

    // Start is called before the first frame update 
    void Start()
    {
        //Recogemos el NavMeshAgent
        agent = GetComponent<NavMeshAgent>();

        //Recogemos el speed del ScripteableObject
        agent.speed = aIObject.Speed;
        //Recogemos el StopDistance del ScripteableObject
        agent.stoppingDistance = aIObject.StopDistance;
         //Recogemos el paths del ScripteableObject
        paths.AddRange(aIObject.Paths);
        //destinationPosition por defecto el primero del Array
        destinationPosition = paths[0];
        //Buscamos al jugador por el tag
        trPlayer = GameObject.FindGameObjectWithTag("Player").transform;
    }

    // Update is called once per frame
    void Update()
    {
        //Estados de la maquina
        StatesMachines();
        //Ponemos el destino al NavMeshAgent
        agent.SetDestination(destinationPosition);
    }

    //Estados la IA
    private void StatesMachines(){
        switch(states){
            case States.Route:
                Route();
                break;
            case States.FollowPlayer:
                FollowPlayer();
                break;
            case States.Attack:
                Attack();
                break;
            default:
                Route();
                break;
        }
    }

    //Funcion que usaremos para ver el player
    private void ViewPlayer(){
        //Calculamos la distancia desdel player y la IA
        float distance = Vector3.Distance(transform.position,trPlayer.position);
        //Si distancia es menor o igual que la distancia de vision
        if(distance <= aIObject.ViewDistance){
            //Calculamos la direccion
            Vector3 direction = trPlayer.position-transform.position;
            //Con esa direccion calculamos el angulo
            float angle = Vector3.Angle(transform.forward,direction);
            //Si el angulo esta entre el minimo y el maximo
            if(angle >= aIObject.MinAngleView && angle <= aIObject.MaxAngleView){
                //Lanzamos un Raycast
                RaycastHit hit;
                if(Physics.Raycast(transform.position,transform.forward,out hit,aIObject.ViewDistance)){
                    //Comprobamos si es el jugador
                    if(hit.collider.CompareTag("Player")){
                        //Cambiamos al estado de persiguiendo al jugador
                        states = States.FollowPlayer;
                    }
                }
            }
        //Si no
        }else{
            //Ponemos el estado en Ruta
            states = States.Route;
        }
    }

    //Funcion que persigue al jugador
    private void FollowPlayer()
    {
        //Calculamos la distacnia
        float distance = Vector3.Distance(trPlayer.position,transform.position);
        //Si esta dentro de la distancia de vision
        if(distance<=aIObject.ViewDistance){
            //Si esta dentro de la distancia de parada
            if(distance <= aIObject.StopDistance){
                //Pasamos al estado de atacando
                states = States.Attack;
            //Si no
            }else{
                //Ponemos la posicion de destino la del jugador
                 destinationPosition = trPlayer.position;
            }
        }
        //Si no
        else{
            //Ponemos el estado de ruta
            states = States.Route;
        }
    }

    //Funcion de Ataque
    private void Attack()
    {
        //Calculamos la distancia
        float distance = Vector3.Distance(trPlayer.position,transform.position);
        //Si esta entre la distancia de parada
        if(distance <= aIObject.StopDistance){
            //Ataca
            Debug.Log("ATACA AL JUGADOR");
        }
        //Si no
        else{
            //Cambiamos al estado de persiguiendo al jugador
            states = States.FollowPlayer;
        }
    }

    //Funcion de ruta
    private void Route(){
        //Ponemos la posicion de la ruta la posicio del array path que tenemos guardada
        destinationPosition = paths[idPos];
        //Calculamos la distancia
        float distance = Vector3.Distance(transform.position,destinationPosition);
        //Si no hemos lanzado la corrutina y la distancia es menor o igual que la distancia de parada
        if(distance<=agent.stoppingDistance && coroutineGoToRoute == null){
            //Lanzamos la corrutina
            coroutineGoToRoute = StartCoroutine(GoToRoute());
        }
        //Comprobamos si vemos al jugador
        ViewPlayer();
    }

    //Corrutina de ir a la ruta
    private IEnumerator GoToRoute(){
        //Esperamos un tiempo
        yield return new WaitForSeconds(aIObject.TimeInPosition);
        //Incrementamos la posicion del array de pathas
        idPos++;
        //Si es mayor que la longitud de la lista la ponemos a  0
        idPos = (idPos<paths.Count) ? idPos : 0;
        //Reseteamos la corrutina
        coroutineGoToRoute = null;
    }
}
