using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class MyPaths : MonoBehaviour
{

    //ScripteableObject
    [SerializeField] private AIObject aIObject;

    // Start is called before the first frame update
    void Start()
    {
        //Si el Paths esta vacio
        if(aIObject.Paths.Length.Equals(0)){
            //Creamos un array vacio de tamanio como hijos tenga el GameObject
            Vector3[] paths = new Vector3[transform.childCount];
            //Despues hacemos un bucle como tanto hijos tenga
            for(int i = 0; i < transform.childCount; i++)
            {
                //Guardamos la posicion de los hijos en el array
                paths[i] = transform.GetChild(i).position;
            }
            //Guaramos el array en el ScripteableObject
            aIObject.Paths = paths;
        }
    }
}
