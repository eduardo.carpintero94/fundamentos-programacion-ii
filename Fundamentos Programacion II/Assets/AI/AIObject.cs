using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[CreateAssetMenu(fileName = "AI",menuName = "My ScripteableObjects/AI", order = 20)]
public class AIObject : ScriptableObject
{
    //Variable de velocidad
    [SerializeField] private float speed;
    //Variable de distancia de parada
    [SerializeField] private float stopDistance;
    //Variable de tiempo en la posicion
    [SerializeField] private float timeInPosition;
    //Variable de distancia de vision
    [SerializeField] private float viewDistance;
    //Minimo angulo de vision
    [SerializeField] private float minAngleView;
    //Maximo angulo de vision
    [SerializeField] private float maxAngleView;
    // Array con la posiciones de la ruta
    [SerializeField] private Vector3[] paths;

    public float Speed { get => speed; set => speed = value; }
    public float StopDistance { get => stopDistance; set => stopDistance = value; }
    public Vector3[] Paths { get => paths; set => paths = value; }
    public float TimeInPosition { get => timeInPosition; }
    public float ViewDistance { get => viewDistance; }
    public float MinAngleView { get => minAngleView; }
    public float MaxAngleView { get => maxAngleView; }
}
