using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.InputSystem;
using Cinemachine;
using UnityEngine.Animations.Rigging;
using System;

public class PlayerZoom : MonoBehaviour
{
    [SerializeField] private float speedRotation = 100f;
    //Cabecera para ordenar nuestras variables en el editor
    [Header("Cinemachine")]
    //Cogemos el cinemachine de tercerapersona
    [SerializeField] private CinemachineFreeLook cmThirdPerson;
    //Cogemos el cinemachine de zoom
    [SerializeField] private CinemachineVirtualCamera cmZoom;
    //Cabecera para ordenar nuestras variables en el editor
    [Header("Canvas")]
    //Canvas para mostrar donde estamos apuntando
    [SerializeField] private GameObject canvasZoom;
    //Cabecera para ordenar nuestras variables en el editor
    [Header("Rigging")]
    //Target para rotar el Spine2
    [SerializeField] private Transform spineTarget;
    //Rotacion minima
    [SerializeField] private float minRotation;
    //Rotacion maxima
    [SerializeField] private float maxRotation;
    //Rig que controla la ik del Spine
    [SerializeField] private Rig rigSpine;

    //Variable que pondremos  a true cuando haremos zoom
    private bool inZoom = false;
    //Direccion del mouse
    private Vector2 directionLook = Vector2.zero;
    //Player Controller para recoger la direccion de movimiento
    private PlayerController playerController;
    
    //Variables donde guardamos las rotaciones
    private float rotationY = 0;
    private float rotationX = 0;

    private void Start()
    {
        //Recogemos el PlayerController del jugador
        playerController = GetComponent<PlayerController>();
    }

    private void Update()
    {
        //Si hacemos Zoom
        if (inZoom)
        {
            //Cambiamos la rotacion horizontal
            HorizontalRotation();
            //Cambiamos la rotacion vertical
            VerticalRotation();
            //Cambiamos la rotacion del SpineTarget
            spineTarget.rotation = Quaternion.Euler(rotationY,rotationX,0);
        }
        //Si no hacemos zoom
        else
        {
            //Ponemos la rotacion de Spine Target a 0
            spineTarget.rotation = Quaternion.identity;
        }
    }

    //Metodo que gestiona la rotacion vertical
    private void VerticalRotation()
    {
        //Multiplicamos  la direccion de rotacion , la velocidad y  deltatime
        rotationY -= directionLook.y * speedRotation * Time.deltaTime;
        //Despues lo limitamos a un maximo y un minimo
        rotationY = Mathf.Clamp(rotationY, minRotation, maxRotation);
    }

    //Metodo que gestiona la rotacion horizontal
    private void HorizontalRotation()
    {
        //Multiplicamos una direccion, la velocidad de rotacion y el time delta time
        //Si directionLook.x no es 0, recogemos su valor y si no cogemos de playerController directionMove.X
        rotationX += (directionLook.x != 0) ? directionLook.x : playerController.directionMove.x 
            * speedRotation * Time.deltaTime;
        //Le pasamos al personaje la rotacion
        transform.rotation = Quaternion.Euler(Vector3.up * rotationX);
    }

    //Metodo del input del zoom
    private void OnZoom(InputValue value)
    {
        //Cambiamos la camara
        SwitchComponents(value.isPressed);
        //Pasamos el valor a inzoom
        inZoom = value.isPressed;
        //Actibamos o desactivamos el canvas segundo pulsamos el input
        canvasZoom.SetActive(value.isPressed);
    }

    //Funcion del Input para recoger el movimiento del raton
    private void OnLook(InputValue value)
    {
        //Recogemos la direccion del raton
        directionLook = value.Get<Vector2>();
    }

    //Funcion para cambiar la camara
    private void SwitchComponents(bool zoom)
    {
        //Si hacemos zoom
        if (zoom)
        {
            //Ponemos la prioridad de cinemachine tercera persona a 0
            cmThirdPerson.Priority = 0;
            //Ponemos la prioridad de cinemachine de zoom a 1
            cmZoom.Priority = 1;
            //Cambiamos el weight del rig del spine a 1
            rigSpine.weight = 1;
        }
        //Si no hacemos zoom
        else
        {
            //Ponemos la prioridad de cinemachine tercera persona a 1
            cmThirdPerson.Priority = 1;
            //Ponemos la prioridad de cinemachine de zoom a 0
            cmZoom.Priority = 0;
            //Cambiamos el weight del rig del spine a 0
            rigSpine.weight = 0;
        }
    }
}
