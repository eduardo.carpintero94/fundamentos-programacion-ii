using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using TMPro;
using UnityEngine.Animations.Rigging;

[RequireComponent(typeof(Recoil))]
public class Gun : MonoBehaviour
{
    //Posicion de disparo
    [SerializeField] private Transform trPos;
    //Scripteable Objecto con la informacion del arma
    [SerializeField] private GunObject gunObject;
    //TextMesh para mostar la direccion
    [SerializeField] private TextMeshPro textMesh;
    [SerializeField] private GameObject bulletHole;
    [SerializeField] private GameObject flare;
    [Header("Rigging")]
    [SerializeField] private Transform rightIkTarget;
    [SerializeField] private Transform rightHinIkTarget;
    [SerializeField] private Transform leftIkTarget;
    [SerializeField] private Transform leftHinIkTarget;
    
    [HideInInspector] public TwoBoneIKConstraint rightIkConstraint;
    [HideInInspector] public TwoBoneIKConstraint leftIkConstraint;

    //Variable boolena que usmaos para contorlar si se dispara o no
    private bool shooting = false;
    //Retroceso del arma
    private Recoil recoil;
    //AudioSource con el que ejecutaremos el sonido del arma
    private AudioSource audioSource;

    // Start is called before the first frame update
    void Start()
    {
        //Recogemos el script de retroceso
        recoil = GetComponent<Recoil>();
        //Igualamos la municion a la municion maxima
        gunObject.Ammo = gunObject.MaxAmmo;

        //Actualizmaos el texto con la municion
        textMesh.text = gunObject.Ammo + "/" + gunObject.MaxAmmo;

        //Recogemos el audioSource
        audioSource = GetComponent<AudioSource>();
    }

    // Update is called once per frame
    void FixedUpdate()
    {

        rightIkConstraint.data.target.position = rightIkTarget.position;
        rightIkConstraint.data.target.rotation = rightIkTarget.rotation;

        rightIkConstraint.data.hint.position = rightHinIkTarget.position;
        rightIkConstraint.data.hint.rotation = rightHinIkTarget.rotation;

        leftIkConstraint.data.target.position = leftIkTarget.position;
        leftIkConstraint.data.target.rotation = leftIkTarget.rotation;

        leftIkConstraint.data.hint.position = leftHinIkTarget.position;
        leftIkConstraint.data.hint.rotation = leftHinIkTarget.rotation;

        //Si shooting es true y la municion es mayor que 0
        if (shooting && gunObject.Ammo > 0)
        {
            //Segun el tipo de disparo que tengamos guardado en los ScripteablesObjects
            switch (gunObject.GunShootType)
            {
                case GunObject.ShootType.oneShoot:
                    OneShoot(trPos.forward);
                    //Dejamos de disparar
                    shooting = false;
                    break;
                case GunObject.ShootType.multipleShoot:
                    MultipleShoot();
                    break;
                case GunObject.ShootType.shootgun:
                    Shotgun();
                    //Dejamos de disparar
                    shooting = false;
                    break;
            }
        }
    }

    //Funcion de la Escopeta
    private void Shotgun()
    {
        //Propagacion horizontal
        float horizontalSpread = 0.25f;
        //Propagacion vertical
        float verticalSpread = 0.25f;
        //Bucle con el numero de disparos
        for (int i = 0; i < 8; i++)
        {
            //Angulo aleatorio en radianles
            float rad = Random.Range(0.0f, 360f);
            //Propagacion en el eje X aleatorio multiplicado con el coseno
            float spreadX = Random.Range(0, horizontalSpread) * Mathf.Cos(rad);
            //Propagacion en el eje Y aleatorio multiplicado con el seno
            float spreadY = Random.Range(0, verticalSpread) * Mathf.Sin(rad);
            //Creamos la desviacion
            Vector3 desviation = new Vector3(spreadX, spreadY, 0);
            //Disparamos en la direccion + la desviacion
            OneShoot(trPos.forward+desviation);
        }
    }

    //Tiempo de cada disparo
    private float timer = 0;
    //Funcion que dispara cada X tiempo
    private void MultipleShoot()
    {
        //Si el tiempo es menor a 0.25 segundos
        if (timer < 0.25f)
        {
            //Incrementamos la variable timer
            timer += Time.deltaTime;
        }
        //Si no disparamos
        else
        {
            //Disparamos
            OneShoot(trPos.forward);
            //Reseteamos la variable
            timer = 0;
        }
    }

    private void OneShoot(Vector3 direction)
    {
        //Informacion del ray cast
        RaycastHit hit;
        //Tiramos un raycast para ver hacia donde se ha disparado
        if (Physics.Raycast(trPos.position, direction, out hit,gunObject.Range))
        {
            //Posicion donde aparecer el agujero de bala segun la posicion de colision de rayo
            //+ la normal multiplacdo por el offset
            Vector3 pos = hit.point+(hit.normal*0.01f);
            //Calculamos la rotacion segun la normal
            Quaternion rot = Quaternion.FromToRotation(transform.up, hit.normal);

            //Instanciamos el agujero de bala con la posicion y rotacio calculada
            GameObject bulletHoleClone = Instantiate(bulletHole,pos,rot);
            //Enparentamos al objeto donde el rayo colisiono
            bulletHoleClone.transform.parent = hit.collider.transform;
        }
        //Iniciamos el retroceso
        recoil.StartRecoil();
        //Disminuimos el arma
        gunObject.Ammo--;
        //Actualziamos el texto
        textMesh.text = gunObject.Ammo + "/" + gunObject.MaxAmmo;
        //Ejecutamos el sonido
        audioSource.PlayOneShot(gunObject.ShootSound);

        //Actibamos el flare
        flare.SetActive(true);
        //Ejecutamos la funcion pero dentro de 0.25 segundos
        Invoke("DisableFlare",0.25f);
    }
    private void DisableFlare()
    {
        //Desactivamos el flare
        flare.SetActive(false);
    }

    //Funcion para iniciar el disparo
    public void StartShooting(bool isPressed)
    {
        shooting = isPressed;
    }
}
