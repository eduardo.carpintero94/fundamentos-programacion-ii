using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Recoil : MonoBehaviour
{
    //Tiempo del retroceso
    [SerializeField] private float timeRecoil;
    //Distancia de movimiento del retroceso
    [SerializeField] private Vector3 distanceRecoil;
    //Corrutina del retroceso
    private Coroutine recoilCoroutine;

    //Corrutina para mover el objeto
    private IEnumerator MoveObject(Vector3 pointA,Vector3 pointB,float duration)
    {
        //Iniciamos el tiempo
        float time = 0;
        //Si el tiempo es menor que la duracion
        while (time<duration)
        {
            //Hacemos una interpolacion lineal desde el punto A al punto B
            //La velocidad la ajusta con el tiempo dividido con la duracion
            transform.localPosition = Vector3.Lerp(pointA, pointB, time / duration);
            //Incrementamos el time con el tiempo
            time += Time.deltaTime;
            yield return null;
        }
    }

    //COrrutina para controlar el retroceso
    private IEnumerator RecoilCorountine()
    {
        //Posicion inicial del retroceso
        Vector3 startPosition = transform.localPosition;
        //La posicion final es la posicion inicial + la distancia de retroceso
        Vector3 endPosition = startPosition + distanceRecoil;

        //Inicialos la corrutina para que vaya del punto A al punto B
        yield return StartCoroutine(MoveObject(startPosition,endPosition,timeRecoil/2));
        //Cuando haya acabado la corrutina iniciamos otra que vaya del punto B al punto A
        yield return StartCoroutine(MoveObject(endPosition, startPosition, timeRecoil / 2));
        //Reseteamos la corrutina
        recoilCoroutine = null;
    }

    //Funcion para iniciar la corrutina
    public void StartRecoil()
    {
        //Si es nulo, es que no se ha iniciado la corrutina
        if(recoilCoroutine == null)
        {
            //Iniciamos la corrutina
            recoilCoroutine =  StartCoroutine(RecoilCorountine());
        }
    }
}
