using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class IkCharacter : MonoBehaviour
{
    //Objeto para mover la parte del cuerpo a esa posicion
    [SerializeField] private Transform target;
    //Animator al que mover la IK
    [SerializeField] private Animator animator;
    //Minimo y Maximo que obtendra el valor weightLookAt
    [Range(0, 1)]
    //Valor que pondremos el peso de la animacion
    [SerializeField] private float weightLookAt = 1;

    private void OnAnimatorIK(int layerIndex)
    {
        //Aplicamos el peso al lookAt
        animator.SetLookAtWeight(weightLookAt);
        //Con esto hacemos que mire al target
        animator.SetLookAtPosition(target.position);

        //Aplicamos el peso a la mano derecha
        animator.SetIKPositionWeight(AvatarIKGoal.RightHand, weightLookAt);
        //Aplicamos la posicion a la mano derecha
        animator.SetIKPosition(AvatarIKGoal.RightHand, target.position);
        //Aplicamos el peso a la mano izquierda
        animator.SetIKRotationWeight(AvatarIKGoal.LeftHand, weightLookAt);
        //Aplicamos la posicion a la mano izquierda
        animator.SetIKRotation(AvatarIKGoal.LeftHand, target.rotation);

    }
}
