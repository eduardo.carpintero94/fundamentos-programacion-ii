using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class GunFolllowPlayer : MonoBehaviour
{
    //Pasamos el Transform del jugador 
    [SerializeField] private Transform trPlayer;


    // Start is called before the first frame update
    void Awake()
    {
        //Emparentamos el arma al jugador
        transform.SetParent(trPlayer);
    }
}
