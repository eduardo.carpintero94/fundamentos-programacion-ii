using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.InputSystem;

public class PlayerController : MonoBehaviour
{
    //Variable para la grabedad
    [SerializeField] private float graviy = -9.8f;
    //Variable para la velocidad
    [SerializeField] private float speed = 5;
    //Variable para la velocidad de rotacion
    [SerializeField] private float rotationSpeed = 1.5f;
    //Fuerza del salto
    [SerializeField] private float jumpForce = 20;
    //Arma seleccionada
    public Gun gunSelected;

    //Variable para guardar el CharacterController
    private CharacterController controller;
    //Variable bool para comprobar si esta tocando el suelo
    private bool inGrounded = false;
    //Varibale para almacenar el movimiento
    private Vector3 move = Vector3.zero;
    //Variable para la direccion del imput
    public Vector2 directionMove = Vector2.zero;
    //Variable para comprobar si hemos pulsado el boton de salto
    private bool jump = false;
    //Variable para guardar el Animator
    private Animator animator;

    //Variable que usaremos para parar poder mover o no el personaje
    private bool start = false;
    //ScriptPlayerZoom
    private PlayerZoom playerZoom;

    //Hace que el personaje pueda moverse
    private void StartGame()
    {
        start = true;

        //Comprobamos si playerZoom no es nulo por si el EventManager ha borrado la variable
        if(playerZoom != null)
        {
            playerZoom.enabled = true;
        }
    }

    //Hace que el personaje deja de moverse
    private void StopGame()
    {
        start = false;
        //Comprobamos si playerZoom no es nulo por si el EventManager ha borrado la variable
        if (playerZoom != null)
        {
            playerZoom.enabled = false;
        }
    }

    private void Awake()
    {
        //Aniadimos las funciones a los eventos en el EventManager
        EventManager.onStartGame += StartGame;
        EventManager.onStopGame += StopGame;
        //Recogemos el componente
        playerZoom = GetComponent<PlayerZoom>();
    }

    // Start is called before the first frame update
    void Start()
    {
        //Obtenemos el CharacterController
        controller = GetComponent<CharacterController>();
        //Obtenemos el Animator
        animator = GetComponent<Animator>();
    }

    // Update is called once per frame
    void Update()
    {
        //Si es true se puede mover el personaje
        if (start)
        {
            //Si el CharacterController esta tocando el suelo
            inGrounded = controller.isGrounded;

            //Si esta tocando el suelo
            if (inGrounded)
            {
                //COgemos el forward y la multiplacoms el eje y de la direccion del input
                move = Vector3.forward * directionMove.y;
                //Lo convertimos en la direccion del personaje
                move = transform.TransformDirection(move);
                //Lo multiplicamos la velocidad
                move *= speed;

                //Rotamos el eje y al personaje con la direccion del eje X del input
                transform.Rotate(Vector3.up * rotationSpeed * Time.deltaTime * directionMove.x);

                //Le aplicamos la variable jump al parametro del salto
                animator.SetBool("jump", jump);
                //Si hemos pulsado salto
                if (jump)
                {
                    //Cogemos la raiz cuadrado con la fuerza del salto, la altura del salto y la grabedad
                    move.y += Mathf.Sqrt(Mathf.Abs(jumpForce * 3 * graviy));
                    //Ponemos el input del salto a falso
                    jump = false;
                }
            }

            //Le ponemos la grabedad al personaje
            move.y += graviy * Time.deltaTime;
            //Movemos el character controller segun lo guardado del move
            controller.Move(move * Time.deltaTime);
            //Le pasamos la direccion del eje y del input al parametro direction de la animacion
            animator.SetFloat("direction", directionMove.y);
        }
        
    }

    //Funcion Input del movimiento
    private void OnMove(InputValue value)
    {
        //Recogemos la direccion de los inputs pulsados
        directionMove = value.Get<Vector2>();
    }

    //Funcion Input del salto
    private void OnJump(InputValue value)
    {
        //Si hemos pulsado el salto
        jump = value.isPressed;
    }

    //Funcion Input del correr
    private void OnRun(InputValue value)
    {
        // Si hemos pulsado la tecla de correr
        if (value.isPressed)
        {
            //Multipplicamos la velocidad por 2
            speed *= 2;
        }
        //Cuando levantamos la tecla de correr
        else
        {
            //Divimos la velocidad entre dos
            speed /= 2;
        }
    }
    //Funcion Input de disparo
    private void OnFire(InputValue value)
    {
        if (start)
        {
            //Llamamos al shooting del arma
            gunSelected.StartShooting(value.isPressed);
        }
        
    }

    //Funcion para pausar el juego
    private void OnPause(InputValue value)
    {
        //Si lo hemos pulsado
        if (value.isPressed)
        {
            //Comprobamos si tiene que pasar o despausar el juego
            MyGameManager.instance.PauseCheck();
        }
    }
}
