using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Persona
{
    //Variable estatica
    public static string variableStatica = "HOLA";
    //Variable pribada
    private string nombre;
    //Variable con su get y su set
    public int Edad { get; set; }

    //Constructor a la que pasamos el nombre y la edad
    public Persona(string nombre, int edad)
    {
        //Damos valor a las variables globales
        this.nombre = nombre;
        this.Edad = edad;

        Debug.Log("Hola soy una persona");
    }

    //Funcion para cambiar el nombre
    public void SetNombre(string nombre)
    {
        this.nombre = nombre;
    }

    //Funcion para obtener el nombre
    public string GetNombre()
    {
        return nombre;
    }

    //Funcion que te dice si la persona tiene mas de 18 anios
    protected virtual bool EsMayor18()
    {
        return Edad>=18;
    }

}
