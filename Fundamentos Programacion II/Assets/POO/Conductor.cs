using System.Collections;
using System.Collections.Generic;
using UnityEngine;

//Hereda de Persona
public class Conductor : Persona
{
    //Variable con solo get
    public bool IsNovel { get;}

    //Constructor que hereda de Persona
    public Conductor(string nombre, int edad,bool isNovel) : base(nombre, edad)
    {
        //Ponemos valor a la variable global
        this.IsNovel = isNovel;

        Debug.Log("Hola soy un conductor");

        Debug.Log("Es mayor de 18 y menor de 85 ? " + EsMayor18());

    }

    //Sobreescribimos la funcion y aniadimos funcionalidad
    protected override bool EsMayor18()
    {
        return base.EsMayor18() && Edad<=85;
    }

}
