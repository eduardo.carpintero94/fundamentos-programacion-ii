using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class TestScript : MonoBehaviour,IMiFunciones,IMiFunciones2
{
   //FUNCIONES IMPLEMENTADAS CON LAS INTERFACES
   /**************************************/
    public Ciclista GetCiclista()
    {
        throw new System.NotImplementedException();
    }

    public string MiMensaje()
    {
        throw new System.NotImplementedException();
    }

    public void MostrarEnPantalla()
    {
        Debug.Log("Hola");
    }

    public void MostrarMensaje()
    {
        throw new System.NotImplementedException();
    }

    public void OnCollisionEnter(Collision collision)
    {
        throw new System.NotImplementedException();
    }

    public void OnCollisionExit(Collision collision)
    {
        throw new System.NotImplementedException();
    }

    public void OnCollisionStay(Collision collision)
    {
        throw new System.NotImplementedException();
    }
    /**************************************/

    // Start is called before the first frame update
    void Start()
    {
        //Creamos los objetos de persona
        Persona edu = new Persona("Edu",25);
        Persona paco = new Persona("Paco", 25);
        //Funcion set para cambiar el nombre
        edu.SetNombre("Juan");
        //Cambiamos la edad
        edu.Edad = 45;

        Debug.Log(edu.GetNombre()+" - "+edu.Edad);
        Debug.Log(paco.GetNombre());

        //Creamos el objeto conductor
        Conductor conductor = new Conductor("Paco", 90,false);
        //Creamos el objeto motoristo
        Motorista motorista = new Motorista("Motorista",26,true,200);
        //Creamos el objeto ciclisto
        Ciclista ciclista = new Ciclista("Ciclista", 27, false);
    }

    // Update is called once per frame
    void Update()
    {
        
    }
}

//Creamos la interfaz
public interface IMiFunciones
{
    //Funciones que se importaran cuando importamos la interfaz
    public void MostrarEnPantalla();
    public void OnCollisionEnter(Collision collision);
    public void OnCollisionStay(Collision collision);
    public void OnCollisionExit(Collision collision);
    public string MiMensaje();
    public Ciclista GetCiclista();
}

//Creamos la interfaz 2
public interface IMiFunciones2
{
    //Funcion que se importara cuando implementemos la interfaz
    public void MostrarMensaje();
}
