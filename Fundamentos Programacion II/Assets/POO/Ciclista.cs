using System.Collections;
using System.Collections.Generic;
using UnityEngine;

//Hereda de Conductor
public class Ciclista : Conductor
{
    //Constructor que hereda de Conductor que a su vez hereda de Persona
    public Ciclista(string nombre, int edad, bool isNovel) : base(nombre, edad, isNovel)
    {
    }
}
