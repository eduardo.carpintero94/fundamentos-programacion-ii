using System.Collections;
using System.Collections.Generic;
using UnityEngine;

//Hereda de conductor
public class Motorista : Conductor
{

    //Estructura donde guardamos los ragos del motorista y implementamos la interfaz
    public struct RasgosMotorista:IMiFunciones
    {
        private Color colorDelPelo;
        private Color colorDeOjos;
        private float altura;

        //Constructor al que le pasamos colorDePelo, colorDeOjos,altura
        public RasgosMotorista(Color colorDelPelo, Color colorDeOjos, float altura)
        {
            this.colorDelPelo = colorDelPelo;
            this.colorDeOjos = colorDeOjos;
            this.altura = altura;
        }

        //Funciones get para recoger las variables globales
        public Color ColorDelPelo { get => colorDelPelo;  }
        public Color ColorDeOjos { get => colorDeOjos;  }
        public float Altura { get => altura;  }

        public Ciclista GetCiclista()
        {
            throw new System.NotImplementedException();
        }

        //FUNCIONES IMPLEMENTADAS DE LA INTERFAZ
        /************************************/
        public string MiMensaje()
        {
            throw new System.NotImplementedException();
        }

        public void MostrarEnPantalla()
        {
            throw new System.NotImplementedException();
        }

        public void OnCollisionEnter(Collision collision)
        {
            throw new System.NotImplementedException();
        }

        public void OnCollisionExit(Collision collision)
        {
            throw new System.NotImplementedException();
        }

        public void OnCollisionStay(Collision collision)
        {
            throw new System.NotImplementedException();
        }
        /************************************/
    }



    //Variable con get y set
    public float KM { get;set; }

    //Constructor al que le aniadimos KM y hereda de Conductor y a la vez de Persona
    public Motorista(string nombre, int edad , bool isNovel,float KM) : base(nombre, edad, isNovel)
    {
        this.KM = KM;

        rasgosMotorista1 = new RasgosMotorista(Color.red, Color.green, 180);
    }

    //Variable con el struct RasgosMotorista
    private RasgosMotorista rasgosMotorista1;

    //Funcion para recoger rasgosMotorista1
    public RasgosMotorista GetRasgosMotorista()
    {
        return rasgosMotorista1;
    }

    
    public void ArrancarMoto()
    {

    }

    public void PararMoto()
    {

    }
}
