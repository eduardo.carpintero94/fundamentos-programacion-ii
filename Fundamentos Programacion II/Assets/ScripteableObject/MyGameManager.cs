using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class MyGameManager : MonoBehaviour
{
    //Instancia estatica de si mismo
    public static MyGameManager instance;
    //Canvas con el selector del arma
    [SerializeField] private GameObject canvasGunSelection;

    // Start is called before the first frame update
    void Awake()
    {
        //Si es nulo
        if(instance == null)
        {
            //Lo igualamos a si mismo
            instance = this;
        }

        
    }

    private void Start()
    {
        //Ejecutamos el evento
        EventManager.StartGame();
    }


    public void PauseCheck()
    {
        //Si el canvas esta activado
        if (canvasGunSelection.activeSelf)
        {
            //Despausamos el juego
            ResumeGame();
        }
        //Si no
        else
        {
            //Pausamos el juego
            PauseGame();
        }
    }

    //Funcion para pausar el juego
    public void PauseGame()
    {
        //Activamos el canvas
        canvasGunSelection.SetActive(true);

        //Paramos el tiempo
        Time.timeScale = 0.0f;
        //Ejecutamos el evento StopGame
        EventManager.StopGame();
    }

    //Funcion para despausar el juego
    public void ResumeGame()
    {
        //Descativamos el canvas de selector de arma
        canvasGunSelection.SetActive(false);
        //Ponemos el timeScale a 1
        Time.timeScale = 1.0f;
        //Ejecutamos el evento StopGame
        EventManager.StartGame();
    }
}
