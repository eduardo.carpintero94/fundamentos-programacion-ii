using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class ButtonGun : MonoBehaviour
{
    //Texto del boton
    public Text txtGun;
    //Imagen del boton
    public Image imgGun;
    //Id del arma
    public int idGun;
}
