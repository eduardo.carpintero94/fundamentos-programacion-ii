using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

//Le asignamos un nombre por defecto y la ruta del menu
[CreateAssetMenu(fileName = "Gun",menuName ="My ScripteableObjects/Gun",order = 10)]
public class GunObject : ScriptableObject
{
    //enum con los tipos de disparo
    public enum ShootType {oneShoot,shootgun,multipleShoot}
    //Nombre del arma
    [SerializeField] private string gunName;
    //Municion
    [SerializeField] private int ammo = 0;
    //Municion Maxima
    [SerializeField] private int maxAmmo = 100;
    //Imagen del arma
    [SerializeField] private Sprite imgGun;
    //Sonido de disparo
    [SerializeField] private AudioClip shootSound;
    //Sonido de recarga
    [SerializeField] private AudioClip shootReload;
    //Tipo de arma
    [SerializeField] private ShootType gunShootType;
    //Y rango del disparo
    [SerializeField] private float range=100;

    public string GunName { get => gunName; }
    public int Ammo { get => (ammo >= 0) ? ammo : 0; set => ammo = value; }
    public int MaxAmmo { get => maxAmmo; }
    public Sprite ImgGun { get => imgGun; }
    public AudioClip ShootSound { get => shootSound; }
    public AudioClip ShootReload { get => shootReload; }
    public ShootType GunShootType { get => gunShootType; }
    public float Range { get => range; }
}
