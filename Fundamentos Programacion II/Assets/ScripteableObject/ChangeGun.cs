using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Animations.Rigging;

public class ChangeGun : MonoBehaviour
{
    [SerializeField] private MenuSelectGun menuSelectGun;

    private void Start()
    {
        EnableGun(0,menuSelectGun.gunGameObjects,menuSelectGun.playerController,
            menuSelectGun.rightIkConstraint,menuSelectGun.leftIkConstraint);
    }

    public void EnableGun(int idGun,GameObject[] gunGameObjects,PlayerController playerController,
        TwoBoneIKConstraint rightIkConstraint, TwoBoneIKConstraint leftIkConstraint)
    {
        for (int i = 0; i < gunGameObjects.Length; i++)
        {
            //gunGameObjects[i].SetActive((i==idGun));

            GameObject gunObjectSelected = gunGameObjects[i];
            if (i == idGun)
            {
                gunObjectSelected.SetActive(true);
                Gun gun = gunObjectSelected.GetComponent<Gun>();
                playerController.gunSelected = gun;

                gun.rightIkConstraint = rightIkConstraint;
                gun.leftIkConstraint = leftIkConstraint;

            }
            else
            {
                gunObjectSelected.SetActive(false);
            }
        }
    }
}
