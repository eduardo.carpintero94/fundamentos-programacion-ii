using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Player : MonoBehaviour
{
    //Varible para iniciar o parar el movimiento del jugador
    public bool start = false;

    // Start is called before the first frame update
    void Start()
    {
        //Anidiamos las funciones a los eventos
        EventManager.onStartGame += StartBallGame;
        EventManager.onStopGame += StopBallGame;
    }

    // Update is called once per frame
    void Update()
    {
        //Si start es igual true
        if (start)
        {
            //Movemos adelante el jugador
            transform.Translate(Vector3.forward * 5 * Time.deltaTime);
        }
        //Si pulsamos C borramos los eventos
        /*if (Input.GetKeyDown(KeyCode.C))
        {
            EventManager.onStartGame -= StartBallGame;
            EventManager.onStopGame -= StopBallGame;
        }*/
    }

    //Funcion para poner start a true
    private void StartBallGame()
    {
        start = true;
    }

    //Funcion para poner start a false
    private void StopBallGame()
    {
        start = false;
    }
}
