using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class EventManager
{
    //Funcion para delegar nuestros eventos en OnStartGame
    public delegate void OnStartGame();
    //Creamos el evento
    public static event OnStartGame onStartGame;

    //Funcion para ejecutar el evento
    public static void StartGame()
    {
        if(onStartGame != null)
        {
            onStartGame();
        }
    }

    //Funcion para delegar nuestros eventos en OnStopGame
    public delegate void OnStopGame();
    //Creamos el evento
    public static event OnStopGame onStopGame;

    //Funcion para ejecutar el evento
    public static void StopGame()
    {
        if(onStopGame != null)
        {
            onStopGame();
        }
    }
}
