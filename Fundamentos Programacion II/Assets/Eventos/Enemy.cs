using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Enemy : MonoBehaviour
{
    //Varible para iniciar o parar el movimiento del jugador
    private bool start = false;

    // Start is called before the first frame update
    void Start()
    {
        //Anidiamos las funciones a los eventos
        EventManager.onStartGame += StartCubeGame;
        EventManager.onStopGame += StopCubeGame;
    }

    // Update is called once per frame
    void Update()
    {
        //Si start es igual true
        if (start)
        {
            //Movemos arriba el jugador
            transform.Translate(Vector3.up * 5 * Time.deltaTime);
        }
    }

    //Funcion para poner start a true
    private void StartCubeGame()
    {
        start = true;
    }

    //Funcion para poner start a false
    private void StopCubeGame()
    {
        start = false;
    }
}
