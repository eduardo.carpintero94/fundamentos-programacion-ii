using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class InteractuablesObjects : MonoBehaviour
{
    //Sonido a ejecutar
    [SerializeField]private AudioClip clip;

    protected void Vibrate(){
        //Llamamos al AudioSource del gamemanager y ejecutados el sonido
        GameManager.instance.audioSource.PlayOneShot(clip);
        //Funcion de vibracion
        Handheld.Vibrate();
    }
}
