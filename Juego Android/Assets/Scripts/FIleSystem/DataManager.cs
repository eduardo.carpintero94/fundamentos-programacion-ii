using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System.IO;
using System.Runtime.Serialization.Formatters.Binary;

public class DataManager
{
    //Ruta del archivo
    private static string path = Application.persistentDataPath+"/gameData.edu";
    //Clase con los datos del juego
    private static GameData gameData;
    //Clase para la creacion o lectura de archivos
    private static FileStream fileStream;
    //Clase para volcar los datos de gameData en el archivo
    private static BinaryFormatter bf = new BinaryFormatter();

    //Funcion para inicializar el archivo
    public static void Initializer(){
        //Si el archivo existe
        if(File.Exists(path)){
            //LEEMOS LOS DATOS
            Debug.Log("Datos encontrados");
            //Leemos los datos del archivo
            ReadData();

            Debug.Log("MONEDAS "+gameData.Coins);
        //Si no existe
        }else{
            //CREAMOS LOS DATOS
            Debug.Log("Datos no encontrados");
            //Inicializamos la clase
            gameData = new GameData(0 , 0.0f);
            //Creamos el archivo
            CreateData();
        }
    }

    //Funcion para leer los datos
    public static GameData ReadData(){
        //Abrimos el archivo
        fileStream = new FileStream(path,FileMode.Open);
        //Deserialziamos el archivo y lo convertimos a la clase GameData
        gameData = (GameData) bf.Deserialize(fileStream);
        //Cerramos el archivo
        fileStream.Close();
        //Retornamos gameData
        return gameData;
    }
    //Funcion para crear los datos
    private static void CreateData(){
        //Creamos el archivo
        fileStream = new FileStream(path,FileMode.Create);
        //Metemos los datos de gameData en el archivo
        bf.Serialize(fileStream,gameData);
        //Cerramos el archivo
        fileStream.Close();
    }

    //Funcion para actualizar las monedas
    public static void UpdateCoins(int coins){
        //Cambiamos las monedas de gameData
        gameData.Coins = coins;
        //Escribimos los datos
        CreateData();
    }

    //Funcion para actualizar la distancia
    public static void UpdateDistance(float distance){
        //Cambiamos la distancia
        gameData.Distance = distance;
        //Escribimos los datos
        CreateData();
    }

}
