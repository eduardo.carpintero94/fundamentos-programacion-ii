
//Serializamos la clase para poder almacenar sus datos
[System.Serializable]
public class GameData
{
    //Variables para guardar las monedas
    private int coins;
    //Variable para guardar la distancia
    private float distance;

    public GameData(int coins, float distance)
    {
        this.coins = coins;
        this.distance = distance;
    }

    public int Coins { get => coins; set => coins = value; }
    public float Distance { get => distance; set => distance = value; }
}
