using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class Player : MonoBehaviour
{
    //Variable para mover o parar el player
    private bool start = false;

    //CharacterController para mover al jugador
    private CharacterController controller;
    private int changeDirection = 0;
    private int life = 0;
    private int coin;
    private bool canJump = false;
    private bool inAir = false;
    private Animator animator;
    private AudioSource audioSource;
    private Vector3 direction;

    //Variable velocidad
    [SerializeField] private float speed = 5;
    [SerializeField] private float lateralSpeed = 2;
    [SerializeField] private Text txtLife;
    [SerializeField] private int maxLife = 3;
    [SerializeField] private Text txtCoin;
    [SerializeField] private float gravity = -9.8f;
    [SerializeField] private float forceJump = 2.5f;
    [SerializeField] private SkinnedMeshRenderer[] skinnedMeshRenderers;
    [SerializeField] private float timeFliker = 1.5f;
    [SerializeField] private int totalNumFliker = 5;
    
    //Clase para la lectura y escritura de datos
    private GameData gameData;

    public Text TxtLife { set => txtLife = value; }
    public Text TxtCoin { set => txtCoin = value; }

    private void Awake() {
        //aniadimos los eventos
        EventManager.onStartGame += StartGame;
        EventManager.onStopGame += StopGame;

        life = maxLife;
    }

    // Start is called before the first frame update
    void Start()
    {
        //Recogemos el CharacterController
        controller = GetComponent<CharacterController>();
        animator = GetComponent<Animator>();
        audioSource = GetComponent<AudioSource>();

        
        UpdateTextLife();
        //Recogemos todos los hijos que tengan SkinnedMeshRenderer y 
        //lo guardamos en el Array
        skinnedMeshRenderers = GetComponentsInChildren<SkinnedMeshRenderer>();

        //Leemos los datos
        gameData = DataManager.ReadData();
        //Recogemos las monedas
        coin = gameData.Coins;
        //Actualzimos el texto
        txtCoin.text = coin+" Coins";
    }

    // Update is called once per frame
    void Update()
    {
        //Si stgart es true
        if(start){
            animator.SetFloat("Movement",1);

            if(controller.isGrounded){
                direction.z = speed;
                direction.x = changeDirection*lateralSpeed;
                #if UNITY_EDITOR
                    //Si pulsamos A
                    if(Input.GetKey(KeyCode.A)){
                        //Ponemos X a  -1
                        direction.x = -lateralSpeed;
                    }
                    //Si pulsamos D
                    if(Input.GetKey(KeyCode.D)){
                        //Ponemos X a 1
                        direction.x = lateralSpeed;
                    }
                    if(Input.GetKeyDown(KeyCode.Space)){
                       Jump();
                    }
                #endif

                //Si estamos en el aire
                if(inAir){
                    //Ponemos la animacion de salto a false pero solo se ejcutara cuando toque el suelo
                    animator.SetBool("Jump",false);
                    //Ponemos en el aire a falso
                    inAir = false;
                }
                //Si puede salar
                if(canJump){
                    //Animacion de salto
                    animator.SetBool("Jump",true);
                    //Y Ya no podra saltar
                    canJump = false;
                }
            }else{
                //Si no esta en el suelo esta en el aire
                inAir = true;
            }

            direction.y += gravity*Time.deltaTime;
            //Movemos el CharacterCOntroller
            controller.Move(direction*Time.deltaTime);
        }else{
            animator.SetFloat("Movement",0);
        }
    }

    private void StartGame(){
        start = true;
    }

    private void StopGame(){
        start = false;
    }

    //Funcion que cambia la direccion del personaje
    public void ChangeDirection(int directionToChange){
        changeDirection = directionToChange;
    }

    private void UpdateTextLife(){
        //Aptualizamos la UI
        txtLife.text = "Life: "+life;
    }

    public void ChangeLife(int num,bool damage){
        //Le sumamos a la vida
        life += num;
        
        //Si es menor o igual a 0 paramos el juego
        if(life<=0){
            life = 0;
            EventManager.StopGame();
            GameManager.instance.EnableReloadButton();
            animator.SetTrigger("Death");
            //Actualizamos las monedas
            DataManager.UpdateCoins(coin);
            float totalDistance = GameManager.instance.GetTotalDistance();
            //Si la distancia es mayor que la distancia guardada
            if(totalDistance > gameData.Distance){
                //Actualizamos la distancia
                DataManager.UpdateDistance(totalDistance);
                Debug.Log("NUEVO RECORD");
            }
        //Si hacemos danio
        }else if(damage){
            //Iniciamos la Corrutina
            StartCoroutine(FlikerMesh());
            //Lanzamos el sonido
            audioSource.Play();
        //Si la vida es mayor igual que la vida maxima
        }else if(life >= maxLife){
            //Ponemos la vida igual a la vida maxima
            life = maxLife;
            //Borramos todas las pociones
            foreach(HearthBottle hearthBottle in GameManager.instance.hearthBottles){
                Destroy(hearthBottle.gameObject);
            }
            //Limpiamos la lista de pociones
            GameManager.instance.hearthBottles.Clear();
        }

        UpdateTextLife();
    }

    //COrrutina que parpadea la mesh
    IEnumerator FlikerMesh(){
        //Desactivamos las colisiones
        controller.detectCollisions = false;
        //Bucle para ejecutar las corrutinas de parpadeo
        for(int i=0;i<totalNumFliker;i++){
            //Corrutina que desactiva la mesh
            yield return StartCoroutine(EnableDisableSkinnedMesh(false));
            //El tiempo de espera es tiempoTOtal/Numero de veces
            yield return new WaitForSeconds(timeFliker/totalNumFliker);
            //Corrutina que desactiva la mesh
            yield return StartCoroutine(EnableDisableSkinnedMesh(true));
        }
        //Activamos las colisiones
        controller.detectCollisions = true;
    }

    //Corrutina que activa y desactiva la mesh
    IEnumerator EnableDisableSkinnedMesh(bool enable){
        //Recorremos el array de SkinnedMeshRenderer
        foreach(SkinnedMeshRenderer skinnedMesh in skinnedMeshRenderers){
            //Activamos o desactivamos
            skinnedMesh.enabled = enable;
            //Retornamos null porque hace que espere a que termine el bucle
            yield return null;
        }
    }

    public void AmountSpeed(){
        //Si la velocidad es menor a 21.5 la aumentamos
        if(speed<21.5f){
            speed++;
        }
    }

    public void TakeCoin()
    {
        //Aumentamos las monedas y sumamos +1
        coin++;
        txtCoin.text = coin.ToString()+" coins";;
    }

    //Funcion para llamar en la animacion que ejecuta el salto
    public void StartJumping(){
        direction.y = Mathf.Sqrt(Mathf.Abs(gravity*3*forceJump));
    }
    //Funcion para el boton de salto
    public void Jump(){
        //Si esta en el suelo y anteriormente no ha estado en el aire
        if(controller.isGrounded && !inAir){
            //Puede saltar
            canJump = true;
        }
    }

    //Funcion para obtener la vida
    public int GetLife(){
        return life;
    }  
    //Funcion para obtener la vida maxima
    public int GetMaxLife(){
        return maxLife;
    }
}
