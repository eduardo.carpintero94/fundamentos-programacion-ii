using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.EventSystems;
using UnityEngine.UI;

public class SkinMenu : MonoBehaviour
{
    //Lista de Skin
   [SerializeField] private SkinObject[] skins;
   [SerializeField] private Text txtSkinName;
   [SerializeField] private Text txtLife;
   [SerializeField] private Text txtCoin;
   [SerializeField] private EventTrigger leftEventTrigger;
   [SerializeField] private EventTrigger rightEventTrigger;

   private CameraStarGame cameraStarGame;
   private int idSkin = 0;
   private GameObject playerSkinSelected;

   private void Start() {
       //Buscamos la camara
       cameraStarGame = Camera.main.GetComponent<CameraStarGame>();
        //Buscamos al jugador que este en la escena
       playerSkinSelected = GameObject.FindGameObjectWithTag("Player");
   }

   public void LastSkin(){
        idSkin--;
        //Si la skin es menor que 0 ponemos la ultima posicion del array
        idSkin = (idSkin<0) ? skins.Length-1 : idSkin;
        //Instanciamos el jugador
        InstaciatePlayer();
   }

   public void NextSkin(){
       idSkin++;
       //Si es mayor igual que el array el valor sera 0
       idSkin = (idSkin>=skins.Length) ? 0 : idSkin;
        //Instanciamos el jugador
       InstaciatePlayer();
   }

   private void InstaciatePlayer(){
       //Destruimos el jugador que esta en la escen
       Destroy(playerSkinSelected);

        //Recogemos el ScripteableObject del Skin
        SkinObject skin = skins[idSkin];
        //Mostramos el nombre del Skin
        txtSkinName.text = skin.SkinName;
        //Instanciamos el nuevo jugador
        GameObject playerClone =  Instantiate(skin.Prefab,Vector3.zero,Quaternion.identity);
        //Guardamos el componenete player en una variable
        Player player = playerClone.GetComponent<Player>();
        //Se lo pasamos al GameManager
        GameManager.instance.player = player;
        //Se lo al cameraStarGame como target
        cameraStarGame.target = playerClone.transform;
        
        //Le ponemos el text de Coin y Life
        player.TxtCoin = txtCoin;
        player.TxtLife = txtLife;

        //Aniadimos los eventos a los botones de ir a la izquierda y a la derecha
        AddEventsTriggers(-1,player,leftEventTrigger);
        AddEventsTriggers(1,player,rightEventTrigger);
        //Igualamos la variable con este jugador
        playerSkinSelected = playerClone;
   }

   private void AddEventsTriggers(int direction,Player player,EventTrigger eventTrigger){
       //Creamos la entrada de pulsar
        EventTrigger.Entry entryDown = new EventTrigger.Entry();
        //Le ponemos el tipo de evento que es en este caso de pulsar
        entryDown.eventID = EventTriggerType.PointerDown;
        //Le aniadimos al evento la funcion que esta en player
        entryDown.callback.AddListener((data) => { player.ChangeDirection(direction); });
        //Aniadimos al eventTrigger esta entrada
        eventTrigger.triggers.Add(entryDown);

        //Creamos la entrada de levantar el dedo
        EventTrigger.Entry entryUp = new EventTrigger.Entry();
        //Ponemos que es de tipo levantar
        entryUp.eventID = EventTriggerType.PointerUp;
        //Aniadimos la funcion que esta en player al evento
        entryUp.callback.AddListener((data) => {player.ChangeDirection(0);});
        //Anidimos la entrada al eventTrigger
        eventTrigger.triggers.Add(entryUp);
   }
}
