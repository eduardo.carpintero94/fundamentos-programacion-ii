using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class HearthBottle : InteractuablesObjects
{
    private void OnTriggerEnter(Collider other) {
        if(other.CompareTag("Player")){
            //Sumamos uno de vida
            GameManager.instance.player.ChangeLife(1,false);
            //Funcion que ejecuta el sonido y vibra
            Vibrate();
            //Destruye la pocion
            Destroy(this.gameObject);
        }
    }
}
