using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.SceneManagement;

public class GameManager : MonoBehaviour
{
    //Instancia de GameManager
    public static GameManager instance = null;
    //Variable del suelo a instanciar
    public GameObject floor;
    //Lista de suelos en la escena
    public List<GameObject> floorList = new List<GameObject>();
    public Player player = null;
    //AudioSource que no se ve en el inspector para llamar desde cualquier script
    [HideInInspector]
    public AudioSource audioSource;

    public GameObject bntPause;
    public GameObject btnPlay;
    public GameObject btnReload;
    //Lista de pociones
    public List<HearthBottle> hearthBottles = new List<HearthBottle>();

    private float totalDistance=0;
    private float lastPosPlayer;
    private bool amountedSpeed = false;
    [SerializeField] private Text txtDistance;

    private bool pausedGame = false;

    private void Awake() {
        if(instance == null){
            instance = this;
        }

        //Si El player es nulo lo buscamos
        if(player==null){
            player = GameObject.FindGameObjectWithTag("Player").GetComponent<Player>();
        }

        //Iniciamos los datos de DataManager
        DataManager.Initializer();
    }

    // Start is called before the first frame update
    void Start()
    {
        //Recogemos la ultima posicion del juegador
        lastPosPlayer = player.transform.position.z;
        //Recogemos el audioSource
        audioSource = GetComponent<AudioSource>();
    }

    // Update is called once per frame
    void Update()
    {
        //Si la posicion del jugador no es igual a la ultima registrada
        if(player.transform.position.z != lastPosPlayer){
            //Le sumamos a totalDistance la distancia de las dos posicones
            totalDistance += (player.transform.position.z-lastPosPlayer);
            //Lo mostramos en pantalla
            txtDistance.text = Mathf.FloorToInt(totalDistance)+"m";
            //Guardamos la nueva posicon
            lastPosPlayer = player.transform.position.z;
        }
        //Si el resto es 0
        if(Mathf.FloorToInt(totalDistance%20) == 0){
            //Y no hemos aumentado la velocidad
            if(!amountedSpeed){
                //Aumentamos la velocidad
                player.AmountSpeed();
                //Ponemos a true
                amountedSpeed = true;
            }
        //Si no, lo ponemos a false
        }else{
            amountedSpeed = false;
        }
    }

    //Funcion Play
    public void Play(){
        //Desactivamos el boton play
        btnPlay.SetActive(false);
        //Activamos el boton pause
        bntPause.SetActive(true);
        
        //Si no hemos pausado el juego
        if(!pausedGame){
            //Iniciamos la transicion de la camara
            Camera.main.GetComponent<CameraStarGame>().start = true;
        }else{
            //Iniciamos el evento StartGame
            EventManager.StartGame();
            //Ya no estamos pausando el juego
            pausedGame = false;
        }
        
    }

    //Funcion de pausa del juego
    public void Pause(){
        //Activamos el boton play
        btnPlay.SetActive(true);
        //Desactivamos el boton pause
        bntPause.SetActive(false);
        EventManager.StopGame();

        pausedGame = true;
    }

    //Funcion para recargar el juego
    public void ReloadGame(){
        //Recargamos la escena
        SceneManager.LoadScene(0);
    }

    //Funcion para reactivar el boton de reinicio
    public void EnableReloadButton(){
        //Desactivamos el boton play
        btnPlay.SetActive(false);
        //Descativamos el boton pause
        bntPause.SetActive(false);
        //Activamos el boton de recarga
        btnReload.SetActive(true);
    }

    public float GetTotalDistance(){
        return totalDistance;
    }
}
