using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CameraStarGame : MonoBehaviour
{
    [SerializeField] private Vector3 positionFollowTarget;
    public Transform target;
    [SerializeField] private GameObject GenerateFloor;
    [SerializeField] private float speedMovement = 10f;
    [SerializeField] private FollowPlayer followPlayer;
    [SerializeField] private SkinMenu skinMenu;

    public bool start = false;

    // Start is called before the first frame update
    void Start()
    {
        
    }

    // Update is called once per frame
    void LateUpdate()
    {
        //La camara mira al target
        transform.LookAt(target);
        
        //Si puede empezar a moverse
        if(start){
            //Hacemos una interpolacion lienal hacia la posicion final
            transform.position = Vector3.Lerp(transform.position,positionFollowTarget,speedMovement*Time.deltaTime);
            //Si est cerca de la posicion final
            if(Vector3.Distance(transform.position,positionFollowTarget) <= 0.75f){
                //Colocamos la camara en la posicion final
                transform.position = positionFollowTarget;
                //Activamos el detectador de nivel
                GenerateFloor.SetActive(true);
                //Activamos el script followPlayer
                followPlayer.enabled = true;
                //Desactivamos el menu de seleccion de skin
                skinMenu.gameObject.SetActive(false);
                //Iniciamos el juego
                EventManager.StartGame();
                //Destruimos el componente de la camara
                Destroy(this);
            }
        }
    }
}
