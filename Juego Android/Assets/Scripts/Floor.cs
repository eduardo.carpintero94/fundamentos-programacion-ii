using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Floor : MonoBehaviour
{
    //Probabilidades de aparicion de los objetos
    private float[] probsArray  = {50f,30f,20f};

    //GameObject de obstaculos
    [SerializeField] private GameObject[] obstacles;
    [SerializeField] private GameObject coin;
    [SerializeField] private GameObject hearthBottle;
    //Lista de posiciones donde se instanciara el obstaculo
    [SerializeField] private List<Transform> trPosList = new List<Transform>();

    // Start is called before the first frame update
    void Start()
    {
        //Selecionamos el numero de obstaculos
        int numObstacles = Random.Range(1, 3);
        //Hacemos el bucle segun el numero de obstaculos a generar
        for(int i = 0; i < numObstacles; i++){
            //Selecinamos una posicion de la lista
            int posSelectedId = Random.Range(0, trPosList.Count);
            //Lo guardamos esa posicion en una variable
            Transform trPosSelected = trPosList[posSelectedId];
            //Variable para guardar el objeto a instanciar
            GameObject objetToInstanciate = null;
            //Generamos la probabilidad
            float probabilitySelected = ProbabilitySystem(probsArray);
            switch(probabilitySelected){
                case 0:
                    //Seleccionamos un obstaculo aleatorio
                    objetToInstanciate =  obstacles[Random.Range(0,obstacles.Length)];
                    break;
                case 1:
                    //Seleccioanmos la moneda
                    objetToInstanciate = coin;
                    break;
                case 2:
                    Player player = GameManager.instance.player;
                    //Si la vida es menor a la vida del jugador
                    if(player.GetLife() < player.GetMaxLife()){
                        //Instanciamos las vidas
                        objetToInstanciate = hearthBottle;
                    //Si no
                    }else{
                        //Instanciamos los obstaculos
                        objetToInstanciate = obstacles[Random.Range(0,obstacles.Length)];
                    }
                    break;
                default:
                    //Ponemos que genere un obstaculo por defecto
                    objetToInstanciate =  obstacles[Random.Range(0,obstacles.Length)];
                    break;
            }

            //Instanciamos el obstaculos
            GameObject objectInstanciate=  Instantiate(objetToInstanciate,
                trPosSelected.position, Quaternion.identity, trPosSelected);
            //Si la probabilidad es la vida y tiene el scrit de HearthBottle
            if(probabilitySelected.Equals(2) && objectInstanciate.GetComponent<HearthBottle>() != null){
                //Aniadmos a la lista de GameManager la pocion
                GameManager.instance.hearthBottles.Add(objectInstanciate.GetComponent<HearthBottle>());
            }
            //Borramos la posicion de la lista
            trPosList.Remove(trPosSelected);
        }
    }

    //Funcion que genera un valor segun las probabilidades
    float ProbabilitySystem (float[] probs) {

        float total = 0;

        foreach (float elem in probs) {
            total += elem;
        }

        float randomPoint = Random.value * total;

        for (int i= 0; i < probs.Length; i++) {
            if (randomPoint < probs[i]) {
                return i;
            }
            else {
                randomPoint -= probs[i];
            }
        }
        return probs.Length - 1;
    }
}
