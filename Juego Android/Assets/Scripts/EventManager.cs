using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class EventManager
{
    //Sistema de eventos
    public delegate void OnStartGame();
    public static event OnStartGame onStartGame;
    public static void StartGame(){
        if(onStartGame != null){
            onStartGame();
        }
    }

    public delegate void OnStopGame();
    public static event OnStopGame onStopGame;
    public static void StopGame(){
        if(onStopGame != null){
            onStopGame();
        }
    }
}
