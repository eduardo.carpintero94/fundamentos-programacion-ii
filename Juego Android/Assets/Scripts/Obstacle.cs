using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Obstacle : MonoBehaviour
{

    //Si entra el trigger
    private void OnTriggerEnter(Collider other) {
        //Si entra el jugador
        if(other.CompareTag("Player")){
            //GameManager.instance.player.GetComponent<Player>().ChangeLife(-1);
            //Quitamos vida al jugador
            GameManager.instance.player.ChangeLife(-1,true);
        }
    }
}
