using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class TriggerExtiFloor : MonoBehaviour
{

    private void OnTriggerEnter(Collider other) {
        if(other.CompareTag("GenerateFloor")){
            //Recogemos la ultima posicion de la lista
            int lastFloorIndex = GameManager.instance.floorList.Count-1;
            //Guardamos el transform del ultimo de la lista
            Transform trLastFloor = GameManager.instance.floorList[lastFloorIndex].transform;
            //Recogemos la posicion
            Vector3 pos = trLastFloor.position;
            //Le sumamos Z
            pos.z += 10;
            //Insanciamos el suelo
            GameObject floorClone =  Instantiate(GameManager.instance.floor,pos,Quaternion.identity);
            //Lo aniadimos a la lista
            GameManager.instance.floorList.Add(floorClone);
            //Recogemos el primer elemento de la lista
            GameObject firstFloor = GameManager.instance.floorList[0];
            //Destruimos el primer elemento de la lista
            Destroy(firstFloor);
            //Borramos la primera posicioopn de la lista
            GameManager.instance.floorList.RemoveAt(0);
        }
    }
}
