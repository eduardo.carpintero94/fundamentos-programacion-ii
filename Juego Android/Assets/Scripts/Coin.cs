using System.Collections;
using System.Collections.Generic;
using UnityEngine;

//Heredamos de InteractuableObjects
public class Coin : InteractuablesObjects
{
    private void OnTriggerEnter(Collider other) {
        if(other.CompareTag("Player")){
            //Cogemos la moneda y la destruimos
            GameManager.instance.player.TakeCoin();
            //hacemos vibrar la moneda
            Vibrate();
            //Destruimos la moneda
            Destroy(this.gameObject);
        }
    }
}
