using System.Collections;
using System.Collections.Generic;
using UnityEngine;

//Creamos el elemento del menu
[CreateAssetMenu(menuName="Skin",fileName="Skin",order = 0)]
public class SkinObject : ScriptableObject
{
    [SerializeField] private GameObject prefab;
    [SerializeField] private string skinName;

    //Prefab del jugador
    public GameObject Prefab { get => prefab;}
    //Nombre del Skin
    public string SkinName { get => skinName;}
}
