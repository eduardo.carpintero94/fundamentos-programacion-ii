using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class FollowPlayer : MonoBehaviour
{
    private float offset = 0;
    private Transform trPlayer;

    // Start is called before the first frame update
    void Start()
    {
        //Recogemos el transform del jugador
        trPlayer = GameManager.instance.player.transform;

        //Calculamos la distancia que sera nuestro offset
        offset = trPlayer.position.z - transform.position.z;
    }

    // Update is called once per frame
    void Update()
    {
        //Recogemos la posicon de la camara
        Vector3 cameraPos = transform.position;
        //Cambiamos la Z a la posicion del jugador - el offset
        cameraPos.z = trPlayer.position.z - offset;
        //Le pasamos la posicion a la camara
        transform.position = cameraPos;
    }
}
