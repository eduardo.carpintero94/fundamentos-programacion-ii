using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using Photon.Pun;

public class PhotonUI : MonoBehaviour
{
    [SerializeField] private Text text;
    [SerializeField] private InputField namePlayer;

    // Update is called once per frame
    void Update()
    {
        text.text = "Status: " + PhotonNetwork.NetworkClientState;
    }

    public void ChangePlayerName()
    {
        PhotonNetwork.NickName = namePlayer.text;
        text.gameObject.SetActive(true);
    }
}
