using Photon.Pun;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Bullet : MonoBehaviourPun
{
    [SerializeField] private float speedBullet;

    private Rigidbody rb;
    private Player player;

    // Start is called before the first frame update
    void Start()
    {
        rb = GetComponent<Rigidbody>();
    }

    // Update is called once per frame
    void FixedUpdate()
    {
        rb.velocity = transform.forward * speedBullet*Time.deltaTime;
    }

    private void OnTriggerEnter(Collider other)
    {
        if (other.CompareTag("Player"))
        {
            //other.GetComponent<Player>().SetLife(10);
            player = other.GetComponent<Player>();
            photonView.RPC("SetEnemyLife", RpcTarget.All);
        }

        Destroy(this.gameObject);
    }

    [PunRPC]
    private void SetEnemyLife()
    {
        if(player != null)
        {
            player.SetLife(10);
        }
    }
}
