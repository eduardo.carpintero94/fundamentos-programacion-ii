using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Photon.Realtime;
using Photon.Pun;

public class PhotonConnect : MonoBehaviourPunCallbacks
{
    [SerializeField] private GameObject photonCanvas;
    [SerializeField] private GameObject playerPrefab;

    // Start is called before the first frame update
    void Start()
    {
        PhotonNetwork.AutomaticallySyncScene = false;
        PhotonNetwork.ConnectUsingSettings();
    }

    public override void OnConnected()
    {
        Debug.Log("Conectado");
    }

    public override void OnDisconnected(DisconnectCause cause)
    {
        Debug.Log("Desconectado");
    }

    public override void OnConnectedToMaster()
    {
        Debug.Log("Conectado al master");

        PhotonNetwork.JoinRandomRoom();
    }

    public override void OnJoinRandomFailed(short returnCode, string message)
    {
        Debug.Log("Hubo un error al entrar en una sala aleatoria");

        PhotonNetwork.CreateRoom("MyRoom", new RoomOptions { MaxPlayers = 4 });
    }

    public override void OnJoinedRoom()
    {
        Debug.Log("Se ha conectado a la sala");

        Destroy(photonCanvas);

        PhotonNetwork.Instantiate(playerPrefab.name, Vector3.zero, Quaternion.identity);
    }
}
