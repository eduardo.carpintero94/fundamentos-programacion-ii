using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Photon.Pun;
using TMPro;
using UnityEngine.SceneManagement;

public class Player : MonoBehaviourPun,IPunObservable
{
    [SerializeField] private int life;
    [SerializeField] private float speed = 2;
    [SerializeField] private float speedRotation = 5;
    [SerializeField] private float gravity = -9.8f;
    [SerializeField] private GameObject bullet;
    [SerializeField] private Transform gunTr;
    [SerializeField] private TextMeshPro playerNameText;
    [SerializeField] private TextMeshPro txtPlayerLife;

    private CharacterController controller;
    private Vector3 movement;

    // Start is called before the first frame update
    void Start()
    {
        controller = GetComponent<CharacterController>();

        if (photonView.IsMine)
        {
            Camera.main.transform.parent = transform;
        }

        playerNameText.text = photonView.Owner.NickName;

        foreach(Photon.Realtime.Player player in PhotonNetwork.PlayerList)
        {
            Debug.Log(player.NickName);
        }

        txtPlayerLife.text = life.ToString();
    }

    // Update is called once per frame
    void FixedUpdate()
    {
        if (photonView.IsMine)
        {
            if (controller.isGrounded)
            {
                float vertical = Input.GetAxis("Vertical");
                float horizontal = Input.GetAxis("Horizontal");

                movement = new Vector3(0, 0, vertical);
                movement = transform.TransformDirection(movement);

                transform.Rotate(Vector3.up * speedRotation * horizontal * Time.deltaTime);

            }

            movement.y += gravity * Time.deltaTime;

            controller.Move(movement * speed * Time.deltaTime);
        }
    }

    private void LateUpdate()
    {
        if (photonView.IsMine)
        {
            Fire();
        }
    }


    private void Fire()
    {
        if (Input.GetButtonDown("Fire1"))
        {
            PhotonNetwork.Instantiate(bullet.name, gunTr.position, gunTr.rotation);
        }
    }

    public void OnPhotonSerializeView(PhotonStream stream, PhotonMessageInfo info)
    {
        if (stream.IsWriting)
        {
            stream.SendNext(life);
        }
        else
        {
            life = (int)stream.ReceiveNext();
        }
    }

    public void SetLife(int damage)
    {
        life -= damage;
        txtPlayerLife.text = life.ToString();

        if(life <= 0 && photonView.IsMine)
        {
            PhotonNetwork.Disconnect();
            SceneManager.LoadScene(0);
        }
    }
}
