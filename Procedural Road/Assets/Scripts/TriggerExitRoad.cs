using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class TriggerExitRoad : MonoBehaviour
{
    private void OnTriggerEnter(Collider other) {
        if((other.transform.parent != null && 
                other.transform.parent.CompareTag("Player")) || 
            (other.transform.parent==null && other.CompareTag("Player"))){
            if(LevelGenerator.instance.actualRoad.Equals(0)){
                LevelGenerator.instance.actualRoad++;
            }else{
                LevelGenerator.instance.RemoveFirstRoad();
                LevelGenerator.instance.InstanciateRoads();
            }
            Destroy(this.gameObject);
        }
    }
}
