using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CarController : MonoBehaviour
{
    //Array con los ejes de la ruefa
    [SerializeField] private AxleInfo[] axleInfos;
    //Maxima potencia del motor
    [SerializeField] private float maxMotorTorque = 3000;
    //Maximo angulo de giro
    [SerializeField] private float maxSteeringAngle = 60;

    // Update is called once per frame
    void FixedUpdate()
    {
        //Calculamos la potendia del motor segun el input
        float motor = Input.GetAxis("Vertical")*maxMotorTorque;
        //Calculamos el angulo de giro segun el angulo
        float steering = Input.GetAxis("Horizontal")*maxSteeringAngle;
        foreach(AxleInfo axleInfo in axleInfos){
            //Si es de giro
            if(axleInfo.IsSteering){
                //Ponemos los angulos de giro a las ruedas
                axleInfo.LeftWhellCollider.steerAngle = steering;
                axleInfo.RightWhellCollider.steerAngle = steering;
            }
            //Si es motor
            if(axleInfo.IsMotor){
                //Ponemos la potencia de motor a las ruedas
                axleInfo.LeftWhellCollider.motorTorque = motor;
                axleInfo.RightWhellCollider.motorTorque = motor;
            }
            //Actualizamos la maya 3D de las ruedas
            UpdateMeshWhell(axleInfo.LeftWhellCollider);
            UpdateMeshWhell(axleInfo.RightWhellCollider);
        }
    }

    private void UpdateMeshWhell(WheelCollider wheelCollider){
        //Recogemos el transform de la maya de la rueda
        //Que es su primer hijo
        Transform whellMeshTr = wheelCollider.transform.GetChild(0);

        Vector3 pos;
        Quaternion rot;
        //Calculamos la posicion y la rotacion de la rueda
        wheelCollider.GetWorldPose(out pos,out rot);
        //Se lo pasamos al transform de la maya de la rueda
        whellMeshTr.position = pos;
        whellMeshTr.rotation = rot;
    }
}

[System.Serializable]
public class AxleInfo{
    //WheelCOlliders de la rueda
    [SerializeField] private WheelCollider leftWhellCollider;
    [SerializeField] private WheelCollider rightWhellCollider;
    //Si es motor
    [SerializeField] private bool isMotor;
    //Si es de giro
    [SerializeField] private bool isSteering;

    public WheelCollider LeftWhellCollider { get => leftWhellCollider;}
    public WheelCollider RightWhellCollider { get => rightWhellCollider;}
    public bool IsMotor { get => isMotor;}
    public bool IsSteering { get => isSteering;}
}

