using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class LevelGenerator : MonoBehaviour
{
    public static LevelGenerator instance = null;

    //Diferentes modulos de la carretera
    [SerializeField] private RoarChunk[] roarChunks;
    //Numero de carreteras a instanciar de incicio
    [SerializeField] private int startNumRoards = 5;
    //Lista de carreteras instanciadas
    private List<GameObject> roadsInstanciateList = new List<GameObject>();
    //Direccion de salida de la ultima carretera instanciada
    private RoarChunk.Direction lastRoadDirection;

    private Transform trPlayer;
    public int actualRoad = 0;

    private void Awake() {
        if(instance == null){
            instance = this;
        }
    }

    // Start is called before the first frame update
    void Start()
    {
        trPlayer = GameObject.FindGameObjectWithTag("Player").transform;

        //Generamos la carretera
        GenerateRoads();
    }

    private void GenerateRoads(){
        for(int i=0;i<startNumRoards;i++){
            //Instanciamos la carretera
            InstanciateRoads();
        }
    }

    public void InstanciateRoads()
    {
        //Si la lista esta vacia
        if(roadsInstanciateList.Count.Equals(0)){
            //Seleccionamos una carretera al azar
            int randomRoadsId = Random.Range(0,roarChunks.Length);
            RoarChunk roarChunk = roarChunks[randomRoadsId];
            //La creamos en la poscion 0
            CreateRoad(roarChunk,Vector3.zero);

            Transform firstRoad = roadsInstanciateList[0].transform;
            for(int i=0;i<firstRoad.childCount;i++){
                if(firstRoad.GetChild(i).name.Equals("StartPos")){
                    trPlayer.position = firstRoad.GetChild(i).position;
                    trPlayer.rotation = firstRoad.GetChild(i).rotation;
                    break;
                }
            }
        //Si no esta vacia
        }else{
            //Creamos la variables iniciales
            RoarChunk roarChunk = null;
            Vector3 chunkSize = Vector3.zero;
            /*
            Segun la direccion de salida de la ultima carretera.
            La direccion de entrada sera la contraria de la ultima direccion de salidad
            Por ejemplo: direccion de salidad, sera el norte entonces la entrada sera el sur
            El tamanio que se aplica a la posicion a instanciar dependera de la direccion
            Si es norte-surth sera en el eje Z
            Si es este-oeste sera en el eje X
            */
            switch(lastRoadDirection){
                case RoarChunk.Direction.East:
                    roarChunk = GetRoadChunk(RoarChunk.Direction.West);
                    chunkSize = new Vector3(roarChunk.RoadSize.x,0,0);
                    break;
                case RoarChunk.Direction.North:
                    roarChunk = GetRoadChunk(RoarChunk.Direction.South);
                    chunkSize = new Vector3(0,0,roarChunk.RoadSize.y);
                    break;
                case RoarChunk.Direction.South:
                    roarChunk = GetRoadChunk(RoarChunk.Direction.North);
                    chunkSize = new Vector3(0,0,roarChunk.RoadSize.y);
                    break;
                case RoarChunk.Direction.West:
                    roarChunk = GetRoadChunk(RoarChunk.Direction.East);
                    chunkSize = new Vector3(-roarChunk.RoadSize.x,0,0);
                    break;
            }
            //Recogemos la posicion del ultima carretera de la lista
            Vector3 lastPosRoad = roadsInstanciateList[roadsInstanciateList.Count-1].transform.position;
            //Le sumamos segun el tamanio previamente calculado
            Vector3 pos = lastPosRoad+chunkSize;
            //Creamos la carretera
            CreateRoad(roarChunk,pos); 
        }
    }

    public void RemoveFirstRoad(){
        GameObject firstRoad = roadsInstanciateList[0];

        roadsInstanciateList.RemoveAt(0);
        Destroy(firstRoad);
    }

    //Esta funcion retorna una carretera al azar, segun la direcicon de entrada que pasemos
    private RoarChunk GetRoadChunk(RoarChunk.Direction direction){
        //Lista donde guardaremos las carreteras seleccionadads
        List<RoarChunk> roadChunksList = new List<RoarChunk>();
        //Recorremos el array de carreteras
        foreach(RoarChunk roarChunk in roarChunks){
            //Si coincide la direccion con la direccion de entrada
            if(direction.Equals(roarChunk.StartDirection)){
                //La aniadimos a la lista
                roadChunksList.Add(roarChunk);
            }
        }
        //retornamos una carretera al azar de la lista
        return roadChunksList[Random.Range(0,roadChunksList.Count)];
    }

    //Creamos la carretera segun cual le pasemos y la posicon
    private void CreateRoad(RoarChunk roarChunk,Vector3 pos){
        //Recogemos una carretera al azar a partir del ScripteableObject
        GameObject road = roarChunk.RoadsPrefab[Random.Range(0,roarChunk.RoadsPrefab.Length)];
        //La instanciamos
        GameObject roadInstanciate = Instantiate(road,pos,Quaternion.identity);
        //aniadimos a la lista
        roadsInstanciateList.Add(roadInstanciate);
        //Guardamos su direccion de salidad
        lastRoadDirection = roarChunk.EndDirection;
    }
}
