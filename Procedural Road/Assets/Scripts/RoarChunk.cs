using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[CreateAssetMenu(menuName = "Roar Chunk",fileName = "RoarChunk",order = 0)]
public class RoarChunk : ScriptableObject
{
    //Direcciones donde se instanciara el modulo de carretera
    public enum Direction {North,South,West,East}

    //Distintos prefabs de esa carretera
   [SerializeField] private GameObject[] roadsPrefab;
   //Tamanio de la carretera
   [SerializeField] private Vector2 roadSize;
   //Direccion de entrada a ese modulo de carretera
   [SerializeField] private Direction startDirection;
   //Direccion de salida de ese modulo de carretera
   [SerializeField] private Direction endDirection;

    public GameObject[] RoadsPrefab { get => roadsPrefab;}
    public Vector2 RoadSize { get => roadSize;}
    public Direction StartDirection { get => startDirection;}
    public Direction EndDirection { get => endDirection;}
}
