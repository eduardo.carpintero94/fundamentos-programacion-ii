using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Animations.Rigging;

public class FootFloor : MonoBehaviour
{
    //Axis para los ejes de rotacion
    private enum Axys {forward,back,up,down,left,right}

    //Transform que nos servira para mover las piernas
    [SerializeField] private Transform trTarget;
    //Offset de posicion
    [SerializeField] private Vector3 offset;
    //Offset para la rotacion
    [SerializeField] private Vector3 offsetRotation;
    //Ejes a los que afectara la direccion de rotacion
    [SerializeField] private Axys axys;
    //Angulo minimo de rotacion
    [SerializeField] private float minAngles;
    //Angulo maximo de rotacion
    [SerializeField] private float maxAngles;
    //Animator donde recogemos el valor de la curva de animacion para aplicar al weight
    [SerializeField] private Animator animator;
    //Nombre del parametro de la animacion
    [SerializeField] private string parameterName;
    //TwoBoneIKConstraint para cambiar al weight
    [SerializeField] private TwoBoneIKConstraint twoBone;

    // Update is called once per frame
    void FixedUpdate()
    {
        //Cambiamos el weight segun el parametro del animator
        twoBone.weight = animator.GetFloat(parameterName);

        //Dibujamos un rayo hacia abajo para comprobar la direccion
        Debug.DrawRay(transform.position, Vector3.down, Color.red);
        //Hit para guardar la informacion del raycast
        RaycastHit hit;
        if(Physics.Raycast(transform.position,Vector3.down,out hit))
        {
            //Ponemos el target en el punto donde colision el rayo y le sumamos el offset
            trTarget.position = hit.point+offset;
            //Direccion de rotacion
            //Por defecto lo ponemos en forward
            Vector3 fromDirection = transform.forward;
            //Segun el eje que tengamos puesto
            switch (axys)
            {
                case Axys.forward:
                    fromDirection = transform.forward;
                    break;
                case Axys.back:
                    fromDirection = -transform.forward;
                    break;
                case Axys.up:
                    fromDirection = transform.up;
                    break;
                case Axys.down:
                    fromDirection = -transform.up;
                    break;
                case Axys.right:
                    fromDirection = transform.right;
                    break;
                case Axys.left:
                    fromDirection = -transform.right;
                    break;
            }

            //Calculamos la rotacion segun la direccion y la normal
            //Y a mayores le summamos el offset
            Quaternion rotationIk = Quaternion.FromToRotation(fromDirection, hit.normal) * Quaternion.Euler(offsetRotation);

            //Limitamos los angulos de los ejes de la rotacion
            Vector3 eulerAnglesIK = rotationIk.eulerAngles;
            eulerAnglesIK.x = Mathf.Clamp(eulerAnglesIK.x, minAngles, maxAngles);
            eulerAnglesIK.y = Mathf.Clamp(eulerAnglesIK.y, minAngles, maxAngles);
            eulerAnglesIK.z = Mathf.Clamp(eulerAnglesIK.z, minAngles, maxAngles);
            rotationIk.eulerAngles = eulerAnglesIK;

            //Le aplicamos la rotacion
            trTarget.rotation = rotationIk;
        }
    }
}
