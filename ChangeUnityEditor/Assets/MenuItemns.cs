using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEditor;

public class MenuItemns
{
    [MenuItem("Edit/Clear Player Prefs")]
    private static void ClearPlayerPrefs()
    {
        PlayerPrefs.DeleteAll();
    }

    [MenuItem("Assets/Get Object Name")]
    private static void GetObjectName()
    {
        var selected = Selection.activeObject;
        Debug.Log(selected.name);
    }

    [MenuItem("CONTEXT/BoxCollider/x2 Size")]
    private static void MultipleBoxCollider()
    {
        GameObject gmCube = Selection.activeGameObject;
        gmCube.GetComponent<BoxCollider>().size *= 2;
    }
}
