using UnityEngine;
using UnityEditor;
using System.Collections.Generic;

public class LevelScript : MonoBehaviour
{
    public int experience;
    public int level { get => experience / 1000; }
    public GameObject playerPrefab;

    // Start is called before the first frame update
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
        
    }
}

[CustomEditor(typeof(LevelScript))]
public class LevelScriptEditor : Editor
{
    private bool canInstanciatePlayer;
    private List<GameObject> playerList = new List<GameObject>();

    public override void OnInspectorGUI()
    {
        EditorGUILayout.HelpBox("Cada 1000 de experiencia, subes un nivel", MessageType.Info);
        LevelScript levelScript = (LevelScript)target;
        levelScript.experience = EditorGUILayout.IntField("Experince", levelScript.experience);
        EditorGUILayout.LabelField("Level", levelScript.level.ToString());

        canInstanciatePlayer = EditorGUILayout.Toggle("Can Instanciate Player", canInstanciatePlayer);
        if (canInstanciatePlayer)
        {
            levelScript.playerPrefab = (GameObject)EditorGUILayout.ObjectField("Player", levelScript.playerPrefab, typeof(GameObject));
            if(levelScript.playerPrefab == null)
            {
                EditorGUILayout.HelpBox("El player no puede ser nulo", MessageType.Error);
            }
            else
            {
                if (GUILayout.Button("Instanciate"))
                {
                    GameObject playerClone = Instantiate(levelScript.playerPrefab, Vector3.zero, Quaternion.identity);
                    playerClone.GetComponent<Player>().level = levelScript.level;
                    playerList.Add(playerClone);
                }
            }
        }

        if (GUILayout.Button("Reset"))
        {
            levelScript.experience = 0;
        }
        if(GUILayout.Button("Clear Players"))
        {
            foreach(GameObject player in playerList)
            {
                DestroyImmediate(player);
            }
            playerList.Clear();
        }
        if(GUILayout.Button("Open Windows"))
        {
            EditorWindow.GetWindow(typeof(MyFirstWindows));
        }
    }
}
