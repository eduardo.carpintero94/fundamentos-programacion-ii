using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEditor;

public class MyFirstWindows : EditorWindow
{
    private GameObject cube;
    private string cubeName;
    private bool groupEnable;
    private float scalerMultipler;
    private bool addRigibody;

    [MenuItem("Tools/My First Windows")]
    private static void ShowWindows()
    {
        EditorWindow.GetWindow(typeof(MyFirstWindows));
    }

    private void OnGUI()
    {
        GUILayout.Label("Cube Settings", EditorStyles.boldLabel);
        cube = (GameObject)EditorGUILayout.ObjectField("Cube", cube, typeof(GameObject));
        if(cube == null)
        {
            EditorGUILayout.HelpBox("El cubo no puede ser nulo", MessageType.Error);
        }
        else
        {
            cubeName = EditorGUILayout.TextField("Cube name", cubeName);
            groupEnable = EditorGUILayout.BeginToggleGroup("Optional Settings", groupEnable);
            scalerMultipler = EditorGUILayout.Slider("Scaler Size",scalerMultipler, 0.01f, 200);
            addRigibody = EditorGUILayout.Toggle("Add Rigibody", addRigibody);
            EditorGUILayout.EndToggleGroup();
            if (GUILayout.Button("Change"))
            {
                cube.name = cubeName;

                if (groupEnable)
                {
                    cube.transform.localScale = cube.transform.localScale * scalerMultipler;

                    if (addRigibody && cube.GetComponent<Rigidbody>() == null)
                    {
                        cube.AddComponent<Rigidbody>();
                    }
                }
            }
        }
    }
}
