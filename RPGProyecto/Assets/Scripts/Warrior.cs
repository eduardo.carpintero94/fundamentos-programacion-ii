using System.Collections;
using System.Collections.Generic;
using TMPro;
using UnityEngine;

//Clase del guerrero
public class Warrior : Character, IAttackCharacter
{
    private float[] probsAttack1 =  {70,20,10};
    private float[] probsAttack2 = { 30, 50, 20 };

    public void Attack1()
    {
        AttackManager.Attack(probsAttack1, weaponSelected.GetWeapon(), textMeshPro, trEnemy.position);
    }

    public void Attack2()
    {
        AttackManager.Attack(probsAttack2, weaponSelected.GetWeapon(), textMeshPro, trEnemy.position);
    }

    public void EndAttack()
    {
        attack = false;
        trailAttack.gameObject.SetActive(false);
    }

    public void EndAttack2(){
        animator.applyRootMotion = false;
        SetGravity();
    }

    protected override void ExecuteAttack2()
    {
        if (CanAttack(15))
        {
            SetZeroGravity();
            animator.applyRootMotion = true;
        }
        
        base.ExecuteAttack2();
    }

    protected override void ExecuteAttack(int valueMana, string animParam)
    {
        if (CanAttack(valueMana))
        {
            trailAttack.gameObject.SetActive(true);
        }
        
        base.ExecuteAttack(valueMana, animParam);
    }

}
