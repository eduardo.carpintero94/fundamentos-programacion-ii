using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using TMPro;

public class FadeOutText : MonoBehaviour
{
    private TextMeshPro textMeshPro;

    [SerializeField] private float speed;
    [SerializeField] private float fadeDuration;

    // Start is called before the first frame update
    void Start()
    {
        textMeshPro = GetComponent<TextMeshPro>();

        StartCoroutine(StartFadeOut());
    }

    private void LateUpdate()
    {
        if (textMeshPro.color.a.Equals(0))
        {
            Destroy(this.gameObject);
        }
        else
        {
            transform.Translate(Vector3.up * speed * Time.deltaTime);
        }
    }

    IEnumerator StartFadeOut()
    {
        yield return StartCoroutine(FadeOut(fadeDuration));

    }

    IEnumerator FadeOut(float duration)
    {
        float time = 0;
        Color32 color = textMeshPro.color;
        byte initialAlpha = color.a; 
        while (time < duration)
        {
            color.a = (byte)Mathf.Lerp((float)initialAlpha, 0f, time / duration);
            textMeshPro.color = color;
            time += Time.deltaTime;

            yield return null;
        }
    }
}
