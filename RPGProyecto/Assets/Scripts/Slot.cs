using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.EventSystems;

//IDropHandler nos implementa el metodo que se ejecuta cuando soltamos un objeto dentor de otro en este
//caso dentor del slot
public class Slot : MonoBehaviour, IDropHandler
{
    private enum SlotType {Weapon,Shield,Storage};

    [SerializeField] private SlotType slotType = SlotType.Storage;
    private CharacterUI characterUI;

    private void Start(){
        characterUI = GetComponentInParent<CharacterUI>();
    }

    //Se ejecuta cuando soltamos un objeto dentro de este
    public void OnDrop(PointerEventData eventData)
    {
        Debug.Log("OnDrop");

        //Cogemos el item del objeto que hemos arrastrado dentro de este
        Item item = eventData.pointerDrag.GetComponent<DragDrop>().item;

        //Variables de weapons y shield
        Weapons weapons = null;
        Shield shield = null;
        //Intentamos convertir el item a weapon
        try{
            weapons = (Weapons) item;
        //Salta InvalidCastException, es decir no ha podido convertirlo a weapon
        }catch(InvalidCastException e){
            //Lo convierte a escudo
            shield = (Shield) item;
        }

        if (slotType.Equals(SlotType.Weapon) && weapons != null)
        {
            PutItem(eventData.pointerDrag);
            characterUI.PutItemRightHand(weapons.ItemGO.name);
        }else if(slotType.Equals(SlotType.Shield) && shield != null)
        {
            PutItem(eventData.pointerDrag);
            characterUI.PutItemLeftHand(shield.ItemGO.name);
        }
        else if(slotType.Equals(SlotType.Storage))
        {
            PutItem(eventData.pointerDrag);
        }

        
    }

    private void PutItem(GameObject pointerDrag)
    {
        //Si pointerDrag es decir el objteo que se arrastra dentro no es nulo
        //Y este objeto no tiene hijos
        if (pointerDrag != null && transform.childCount.Equals(0))
        {
            //Cambiamos el parent del item
            pointerDrag.transform.parent = this.transform;
            //Ponemos la poisicion local del item a 0
            pointerDrag.GetComponent<RectTransform>().localPosition = Vector3.zero;
        }
    }
}
