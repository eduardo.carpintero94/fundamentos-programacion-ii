using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

//Clase para gestionar la interfaz de los personajes
public class CharacterUI : MonoBehaviour

{
    [Header("Life")]
    [SerializeField] private Image healthBar;
    [SerializeField] private Text txtLife;
    [Header("Mana")]
    [SerializeField] private Image manaBar;
    [SerializeField] private Text txtMana;
    [Header("Itemns")]
    [SerializeField] private Item[] items;
    [SerializeField] private GameObject itemToClone;
    [SerializeField] private Transform trPanelItemns;
    [Header("Hands")]
    [SerializeField] private Hand leftHand;
    [SerializeField] private Hand rightHand;

    private void Start() {
        for(int i=0;i<items.Length;i++){
            GameObject itemClone = Instantiate(itemToClone,Vector3.zero,Quaternion.identity);
            itemClone.GetComponent<Image>().sprite = items[i].ItemIcon;

            itemClone.transform.parent  =  trPanelItemns.GetChild(i);
            itemClone.transform.localPosition = Vector3.zero;
            itemClone.transform.localScale = Vector3.one*0.75f;
            itemClone.SetActive(true);
            itemClone.GetComponent<DragDrop>().item = items[i];
        }
    }

    public void UpdatemanaBar(int mana,int manaMax){
        UpdateBar(manaBar,(float) mana,(float) manaMax);
        txtMana.text = mana.ToString();
    }

    public void UpdateHealthBar(int life,int lifeMax){
        UpdateBar(healthBar,(float) life,(float) lifeMax);
        txtLife.text = life.ToString();
    }

    private void UpdateBar(Image image,float value,float valueMax){
        image.fillAmount = value/valueMax;
    }

    public void PutItemLeftHand(string nameItem)
    {
        leftHand.EnableItem(nameItem);
    }

    public void PutItemRightHand(string nameItem)
    {
        rightHand.EnableItem(nameItem);
    }
}
