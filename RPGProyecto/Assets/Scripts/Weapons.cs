using System.Collections;
using System.Collections.Generic;
using UnityEngine;

//ScripteableObject del arma
[CreateAssetMenu(menuName = "Weapon",fileName = "Weapon",order = 0)]
public class Weapons : Item
{
    [SerializeField] private int minDamage;
    [SerializeField] private int maxDamage;
 
    public int MinDamage { get => minDamage;}
    public int MaxDamage { get => maxDamage;}
}
