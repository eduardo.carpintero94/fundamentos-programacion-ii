using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Hand : MonoBehaviour
{
    private enum HandType {Left,Right};

    [SerializeField] private GameObject[] itemns;
    [SerializeField] private HandType handType;

    private Character character;

    private void Awake()
    {
        character = GetComponentInParent<Character>();
        foreach(GameObject item in itemns)
        {
            if (item.activeSelf)
            {
                character.weaponSelected = item.GetComponent<WeaponControll>();
                break;
            }
        }
    }

    public void EnableItem(string nameItem)
    {
        foreach(GameObject item in itemns)
        {
            if (item.name.Equals(nameItem))
            {
                item.SetActive(true);
                switch (handType)
                {
                    case HandType.Left:
                        //////
                        break;
                    case HandType.Right:
                        character.weaponSelected = item.GetComponent<WeaponControll>();
                        character.TrailAttack.transform.SetParent(item.transform);
                        break;
                }
            }
            else
            {
                item.SetActive(false);
            }
        }
    }
}
