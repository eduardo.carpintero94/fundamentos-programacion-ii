using System.Collections;
using System.Collections.Generic;
using UnityEngine;

//GameManager
public class GameManager : MonoBehaviour
{
    public static GameManager instance = null;

    public GameObject enemy;
    public Character player;

    private void Awake() {
        if(instance==null){
            instance = this;
        }
        /*DataManager.Initialize();

        DataManager.gameData.NamePlayer = "Paco";
        DataManager.gameData.Point = 100;
        DataManager.gameData.TimeGame = 20f;

        DataManager.WriteData(DataManager.gameData);*/
    }

    // Start is called before the first frame update
    void Start()
    {
        EventManager.StartGame();
    }

    // Update is called once per frame
    void Update()
    {
        
    }
}
