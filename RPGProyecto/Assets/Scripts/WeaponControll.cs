using System.Collections;
using System.Collections.Generic;
using UnityEngine;

//Clase de programacion de las armas
public class WeaponControll : MonoBehaviour
{
    [SerializeField] private Weapons weapon;
    [SerializeField] private float maxRangeAttack = 10f;
    public bool inRange = false;

    private Transform trPLayer = null;
    private Transform trEnemy;

    private void Awake() {
        trPLayer = GetComponentInParent<Character>().transform;
       
    }

    private void LateUpdate() {
        CheckRange(trPLayer.position);
    }

    public Weapons GetWeapon()
    {
        return weapon;
    }

    public void CheckRange(Vector3 parentPosition)
    {
        trEnemy = GameManager.instance.enemy.transform;

        inRange = (Vector3.Distance(parentPosition, trEnemy.position) <= maxRangeAttack);
    }
}
