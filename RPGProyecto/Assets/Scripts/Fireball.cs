using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Fireball : Projectil
{
    public GameObject explosionPrefab;

    private void OnTriggerEnter(Collider other)
    {
        if (other.CompareTag("Enemy"))
        {
            TriggerProjectil();
            Instantiate(explosionPrefab, transform.position, Quaternion.identity,other.transform);
            Destroy(this.gameObject);
        }
    }
}
