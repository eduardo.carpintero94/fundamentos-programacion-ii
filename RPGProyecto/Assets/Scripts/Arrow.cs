using System.Collections;
using System.Collections.Generic;
using TMPro;
using UnityEngine;

public class Arrow : Projectil
{
    private void OnTriggerEnter(Collider other)
    {
        if (other.CompareTag("Enemy"))
        {
            TriggerProjectil();

            transform.parent = other.transform;
            Destroy(this);
        }
    }
}
