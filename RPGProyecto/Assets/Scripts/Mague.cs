using System.Collections;
using System.Collections.Generic;
using UnityEngine;

//Clase del Mago
public class Mague : Character, IAttackCharacter
{
    [SerializeField] private WeaponControll[] spells;
    [SerializeField] private GameObject fireballExplosionPrefab;

    private float[] probsAttack1 = { 70, 20, 10 };
    private float[] probsAttack2 = { 70, 20, 10 };

    public void Attack1()
    {
        Vector3 fireBallPos = weaponSelected.transform.position;
        Transform fireBallParent = weaponSelected.transform.parent;

        WeaponControll fireBallClone = Instantiate(weaponSelected,fireBallPos,transform.rotation,fireBallParent);
        GameObject fireBallGO = fireBallClone.gameObject;
        Destroy(fireBallClone);

        Rigidbody rbFireball =  fireBallGO.AddComponent<Rigidbody>();
        rbFireball.useGravity = false;
        fireBallGO.GetComponent<Collider>().isTrigger = true;
        Fireball fireballScript = fireBallGO.AddComponent<Fireball>();
        fireballScript.explosionPrefab = fireballExplosionPrefab;
        Weapons fireballScripteableObject = weaponSelected.GetWeapon();
        fireballScript.Initialized(probsAttack1,fireballScripteableObject,textMeshPro,trEnemy.position);

        fireBallGO.transform.parent = null;
        weaponSelected.gameObject.SetActive(false);

        InstaciateTrail(fireBallGO.transform);
    }

    public void Attack2()
    {
        weaponSelected.gameObject.SetActive(true);
    }

    public void EndAttack()
    {
        attack = false;
        weaponSelected.gameObject.SetActive(false);
    }

    protected override void ExecuteAttack1()
    {
        EnableSpellAttack(spells[0],true);
        base.ExecuteAttack1();
    }

    protected override void ExecuteAttack2()
    {
        EnableSpellAttack(spells[1],false);
        base.ExecuteAttack2();
    }

    private void EnableSpellAttack(WeaponControll spell,bool enableGameObject)
    {
        weaponSelected = spell;
        if (enableGameObject)
        {
            weaponSelected.gameObject.SetActive(true);
        }
        weaponSelected.CheckRange(transform.position);
    }

    public void MagueAttack()
    {
        AttackManager.Attack(probsAttack2, weaponSelected.GetWeapon(), textMeshPro, trEnemy.position);
    }
}
