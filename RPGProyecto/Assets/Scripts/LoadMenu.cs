using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.UI;

public class LoadMenu : MonoBehaviour
{
    private enum LoadingStates {Appear,Loading,Disappear}

    public static int idScene = 1;

    [SerializeField] private Text txtLoading;
    [SerializeField] private Image imageLoading;

    private AsyncOperation asyncOperation;
    private CanvasGroup canvasGroup;
    [SerializeField]private LoadingStates loadingStates = LoadingStates.Appear;

    // Start is called before the first frame update
    void Start()
    {
        //StartCoroutine(StartLoadLevel());
        canvasGroup = GetComponent<CanvasGroup>();
        canvasGroup.alpha = 0;

        loadingStates = LoadingStates.Appear;

        DontDestroyOnLoad(this);
    }

    // Update is called once per frame
    void Update()
    {
        if(asyncOperation != null)
        {
            txtLoading.text = "Loading..." + (asyncOperation.progress*100)+"%";
            imageLoading.fillAmount = asyncOperation.progress;

            if (asyncOperation.isDone && loadingStates.Equals(LoadingStates.Loading))
            {
                loadingStates = LoadingStates.Disappear;
            }
        }

        if(loadingStates.Equals(LoadingStates.Appear) && ((int)canvasGroup.alpha).Equals(1))
        {
            loadingStates = LoadingStates.Loading;
        }

        if(loadingStates.Equals(LoadingStates.Disappear) && ((int)canvasGroup.alpha).Equals(0))
        {
            Destroy(this.gameObject);
        }
        
        StatesMachine();
    }

    private IEnumerator StartLoadLevel()
    {
        yield return new WaitForEndOfFrame();
        if(asyncOperation == null)
        {
            asyncOperation = SceneManager.LoadSceneAsync(idScene);
        }
        
    }

    private IEnumerator FadeOut(float endValue,float duration)
    {
        float inicialAlpha = canvasGroup.alpha;
        float time = 0;
        while (time < duration)
        {
            canvasGroup.alpha = Mathf.Lerp(inicialAlpha, endValue, time / duration);
            time += Time.unscaledDeltaTime;

            yield return null;
        }
    }


    private void StatesMachine()
    {
        switch (loadingStates)
        {
            case LoadingStates.Appear:
                StartCoroutine(FadeOut(1, 1.5f));
                break;
            case LoadingStates.Loading:
                StartCoroutine(StartLoadLevel());
                break;
            case LoadingStates.Disappear:
                StartCoroutine(FadeOut(0, 1.5f));
                break;
        }
    }
}
