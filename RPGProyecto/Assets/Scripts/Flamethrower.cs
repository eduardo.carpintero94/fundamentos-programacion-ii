using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Flamethrower : MonoBehaviour
{
    [SerializeField] private float timeDamage = 0.5f;
    [SerializeField] private float maxIntensity = 10;

    private Coroutine coroutine;
    private Mague mague;
    private ParticleSystem particleSystem;
    private Light particleSystemLight;

    private void OnTriggerEnter(Collider other)
    {
        if (other.CompareTag("Enemy"))
        {
            coroutine =  StartCoroutine(FlamethrowerCoruntine());
        }
    }

    IEnumerator FlamethrowerCoruntine()
    {
        yield return new WaitForSeconds(timeDamage);
        mague.MagueAttack();
        coroutine = StartCoroutine(FlamethrowerCoruntine());
    }

    private void LateUpdate()
    {
        if (particleSystemLight.intensity < maxIntensity)
        {
            particleSystemLight.intensity += 0.1f;
        }
    }

    private void OnEnable()
    {
        mague = GetComponentInParent<Mague>();
        particleSystem = GetComponentInChildren<ParticleSystem>();
        particleSystemLight = GetComponentInChildren<Light>();

        particleSystem.Play();
    }

    private void OnDisable()
    {
        if(coroutine != null)
        {
            StopCoroutine(coroutine);
        }

        particleSystem.Stop();
        particleSystemLight.intensity = 0;
    }
}
