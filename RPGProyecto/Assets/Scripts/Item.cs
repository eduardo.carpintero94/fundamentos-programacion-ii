using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Item : ScriptableObject
{
    [SerializeField] private string itemName;
    [SerializeField] private GameObject itemGO;
    [SerializeField] private Sprite itemIcon;

    public string ItemName { get => itemName;}
    public GameObject ItemGO { get => itemGO;}
    public Sprite ItemIcon { get => itemIcon;}
}
