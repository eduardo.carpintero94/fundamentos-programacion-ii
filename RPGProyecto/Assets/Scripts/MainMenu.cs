using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class MainMenu : MonoBehaviour
{
    [SerializeField] private LoadMenu loadMenu;

    public void Play()
    {
        LoadMenu.idScene = 1;
        Instantiate(loadMenu, Vector3.zero, Quaternion.identity);
    }

    public void Exit()
    {
        Application.Quit();
    }
}
