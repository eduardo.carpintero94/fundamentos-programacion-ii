using System.Collections;
using System.Collections.Generic;
using TMPro;
using UnityEngine;

public class Projectil : MonoBehaviour
{
    private Rigidbody rb;
    [SerializeField] private float speed = 500;

    private float[] probs;
    private Weapons weapon;
    private TextMeshPro textMeshPro;
    private Vector3 posEnemy;

    // Start is called before the first frame update
    void Start()
    {
        rb = GetComponent<Rigidbody>();

        Destroy(this.gameObject, 20f);
    }

    // Update is called once per frame
    void Update()
    {
        rb.velocity = transform.forward * speed * Time.deltaTime;
    }

    public void Initialized(float[] probs, Weapons weapon, TextMeshPro textMeshPro, Vector3 posEnemy)
    {
        this.probs = probs;
        this.weapon = weapon;
        this.textMeshPro = textMeshPro;
        this.posEnemy = posEnemy;
    }

    protected void TriggerProjectil()
    {
        rb.constraints = RigidbodyConstraints.FreezeAll;
        GetComponentInChildren<TrailRenderer>().enabled = false;

        AttackManager.Attack(probs, weapon, textMeshPro, posEnemy);

        GameManager.instance.enemy.GetComponent<ChangeColorMaterial>().StartChange(Color.red,10);
    }
}
