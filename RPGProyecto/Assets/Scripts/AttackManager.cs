using System.Collections;
using System.Collections.Generic;
using TMPro;
using UnityEngine;

public class AttackManager
{
    private static float ProvabiliyAttack(float[] probs)
    {

        float total = 0;

        foreach (float elem in probs)
        {
            total += elem;
        }

        float randomPoint = Random.value * total;

        for (int i = 0; i < probs.Length; i++)
        {
            if (randomPoint < probs[i])
            {
                return i;
            }
            else
            {
                randomPoint -= probs[i];
            }
        }
        return probs.Length - 1;
    }

    public static void Attack(float[] probs, Weapons weapon, TextMeshPro textMeshPro, Vector3 posEnemy)
    {
        float damage = 0;
        bool critical = false;
        switch (ProvabiliyAttack(probs))
        {
            case 0:
                damage = weapon.MinDamage;
                break;
            case 1:
                damage = weapon.MaxDamage;
                break;
            case 2:
                damage = weapon.MinDamage + weapon.MaxDamage;
                critical = true;
                break;
        }


        TextMeshPro textMeshProClone = GameObject.Instantiate(textMeshPro, posEnemy, Quaternion.identity);
        textMeshProClone.text = damage.ToString();
        if (critical)
        {
            textMeshProClone.color = Color.yellow;
        }
    }
}
