using System;
using System.Collections;
using System.Collections.Generic;
using System.IO;
using System.Runtime.Serialization.Formatters.Binary;
using UnityEngine;

public class DataManager
{
    private static FileStream file;
    private static BinaryFormatter bf;

    private static string path = Application.persistentDataPath+"/mifichero.sae";
    public static GameData gameData;

    public static void Initialize()
    {
        bf = new BinaryFormatter();

        if (File.Exists(path))
        {
            Debug.Log("Se encontro el archivo");

            gameData = ReadData();

            Debug.Log("El nombre del jugador es " + gameData.NamePlayer);
        }
        else
        {
            Debug.Log("No se encontro el archivo");

            gameData = CreateData();
        }
    }

    private static GameData CreateData()
    {
        file = new FileStream(path, FileMode.Create);

        GameData defaultGameData = new GameData(0, "Player", 0.0f);

        bf.Serialize(file, defaultGameData);

        file.Close();

        return defaultGameData;
    }

    private static GameData ReadData()
    {
        file = new FileStream(path, FileMode.Open);

        GameData loadGameData = (GameData)bf.Deserialize(file);

        file.Close();

        return loadGameData;
    }

    public static void WriteData(GameData playerGameData)
    {
        file = new FileStream(path, FileMode.Create);

        bf.Serialize(file, playerGameData);

        file.Close();
    }
}
