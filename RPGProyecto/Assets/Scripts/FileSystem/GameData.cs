using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[System.Serializable]
public class GameData
{
    private int point;
    private string namePlayer;
    private float timeGame;

    public GameData(int point, string namePlayer, float timeGame)
    {
        this.Point = point;
        this.NamePlayer = namePlayer;
        this.TimeGame = timeGame;
    }

    public int Point { get => point; set => point = value; }
    public string NamePlayer { get => namePlayer; set => namePlayer = value; }
    public float TimeGame { get => timeGame; set => timeGame = value; }
}
