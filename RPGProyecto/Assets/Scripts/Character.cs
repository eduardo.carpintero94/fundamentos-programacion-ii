using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.InputSystem;
using TMPro;

//Clase base de nuestros personajes
[RequireComponent(typeof(CharacterController))]
[RequireComponent(typeof(PlayerInput))]
[RequireComponent(typeof(Animator))]
public class Character : MonoBehaviour
{
    [SerializeField] private int maxLife;
    private int life;
    [SerializeField] private int maxMana;
    private int mana;
    [SerializeField] private float speed;
    [SerializeField] private float rotationSpeed;
    [SerializeField] private float gravity = -9.8f;
    [SerializeField] private CharacterUI characterUI;
    [SerializeField] private GameObject panelSelectWeapon;
    [SerializeField] protected TextMeshPro textMeshPro;
    [SerializeField] protected TrailRenderer trailAttack;

    private bool start = false;
    private CharacterController controller;
    private Vector2 direction = Vector2.zero;
    private Vector3 move;
    protected Animator animator;
    protected bool attack = false;
    protected Transform trEnemy;

    public WeaponControll weaponSelected;

    private void Awake() {
        EventManager.onStartGame += StartGame;
        EventManager.onStopGame += StopGame;

        /*EventManager.onStartGame -= StartGame;
        EventManager.onStopGame -= StopGame;*/
    }

    private void Start() {
        controller = GetComponent<CharacterController>();
        animator = GetComponent<Animator>();

        Initialize();

        trEnemy = GameManager.instance.enemy.transform;
    }

    private void Update() {
        if(start){
            if(controller.isGrounded){
                move = new Vector3(0,0,direction.y);
                move = transform.TransformDirection(move);
                move.z *= speed;

                transform.Rotate(direction.x*transform.up*rotationSpeed*Time.deltaTime);
            }
            move.y += gravity*Time.deltaTime;
            controller.Move(move*Time.deltaTime);

            animator.SetFloat("movement",direction.y);
        }
    }

    private void Initialize()
    {
        life = maxLife;
        mana = maxMana;
    }

    private void StartGame(){
        start = true;
    }

    private void StopGame(){
        start = false;
    }

    private void OnMove(InputValue value){
        direction = value.Get<Vector2>();
    }

    private void OnAttack1(InputValue value){
        ExecuteAttack1();
    }

    public void OnAttack2(InputValue value){
        ExecuteAttack2();
    }

    protected virtual void ExecuteAttack1(){
        ExecuteAttack(5,"attack1");
    }

    protected virtual void ExecuteAttack2(){
        ExecuteAttack(15,"attack2");
    }

    protected virtual void ExecuteAttack(int valueMana,string animParam){
        if(CanAttack(valueMana)){
            transform.LookAt(trEnemy);
            transform.eulerAngles = Vector3.up * transform.eulerAngles.y;

            animator.SetTrigger(animParam);
            SetMana(valueMana);
            attack = true;
        }
    }

    protected bool CanAttack(int valueMana)
    {
        return (!attack && (mana > 0 && valueMana < mana) && (weaponSelected != null && weaponSelected.inRange));
    }

    protected void SetZeroGravity(){
        gravity = 0.0f;
    }

    protected void SetGravity(){
        gravity = -9.8f;
    }

    protected void InstaciateTrail(Transform parent)
    {
        TrailRenderer trailRendererAttackClone = Instantiate(trailAttack, Vector3.zero, Quaternion.identity, parent);
        trailRendererAttackClone.transform.localPosition = Vector3.zero;
        trailRendererAttackClone.gameObject.SetActive(true);
    }

    private void SetMana(int value){
        mana -=value;
        mana = (mana>=0) ? mana : 0;
        characterUI.UpdatemanaBar(mana,maxMana);
    }

    private void OnInventory(InputValue value)
    {
        if (value.isPressed && panelSelectWeapon != null)
        {
            panelSelectWeapon.SetActive(!panelSelectWeapon.activeSelf);
        }
    }

    public void SetDamage(int damage)
    {
        life -= damage;
        characterUI.UpdateHealthBar(life, maxLife);
    }


    public TrailRenderer TrailAttack { get => trailAttack; }
}
