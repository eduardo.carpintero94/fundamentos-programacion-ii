using System.Collections;
using System.Collections.Generic;
using UnityEngine;

//ScripteableObject del escudo
[CreateAssetMenu(menuName = "Shield",fileName = "Shield",order = 0)]
public class Shield : Item
{
   [SerializeField] private int minDefend;
   [SerializeField] private int maxDefend;
    public int MinDefend { get => minDefend;}
    public int MaxDefend { get => maxDefend;}
}
