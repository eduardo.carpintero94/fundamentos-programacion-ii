using System.Collections;
using System.Collections.Generic;
using UnityEngine;

//Clase Arquero
public class Archer : Character, IAttackCharacter
{
    private float[] probsAttack1 = { 70, 20, 10 };
    private float[] probsAttack2 = { 70, 20, 10 };

    [SerializeField] private Arrow arrow;
    [SerializeField] private float rangeMeleeAttack = 2f;

    public void Attack1()
    {
        Arrow arrowClone =  Instantiate(arrow, weaponSelected.transform.position, transform.rotation);
        arrowClone.Initialized(probsAttack1, weaponSelected.GetWeapon(), textMeshPro, trEnemy.position);

        InstaciateTrail(arrowClone.transform);
    }

    public void Attack2()
    {
        AttackManager.Attack(probsAttack2, weaponSelected.GetWeapon(), textMeshPro, trEnemy.position);
    }
    protected override void ExecuteAttack2()
    {
        if (Vector3.Distance(trEnemy.position, transform.position) <= rangeMeleeAttack)
        {
            base.ExecuteAttack2();
        }
        
    }

    public void EndAttack(){
        attack = false;
    }
}

