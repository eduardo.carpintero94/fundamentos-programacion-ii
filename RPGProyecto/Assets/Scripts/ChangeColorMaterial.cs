using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ChangeColorMaterial : MonoBehaviour
{
    private Renderer renderer;

    // Start is called before the first frame update
    void Start()
    {
        renderer = GetComponent<Renderer>();
    }

    private void Change(Color colorToChange,float emissionIntensity)
    {
        renderer.material.SetColor("_EnemyColor", colorToChange);
        renderer.material.SetColor("_EmissionColor", colorToChange);
        renderer.material.SetFloat("_EmissionIntensity", emissionIntensity);
    }

    private IEnumerator ChangeCoruntine(Color colorToChange, float emissionIntensity)
    {
        Change(colorToChange,emissionIntensity);
        yield return new WaitForSeconds(1);
        Change(Color.white, 0);
    }

    public void StartChange(Color colorToChange, float emissionIntensity)
    {
        StartCoroutine(ChangeCoruntine(colorToChange, emissionIntensity));
    }
}
