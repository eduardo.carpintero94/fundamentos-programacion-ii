using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.EventSystems;

//IBeginDragHandler interfaz que implentan el metodo cuando vamos a empezar a arrastar
//IDragHandler interfaz que implentan el metodo cuando estamos arrastrando
//IEndDragHandler interfaz que implentan el metodo cuando terminamos de arrastrar
[RequireComponent(typeof(CanvasGroup))]
public class DragDrop : MonoBehaviour, IBeginDragHandler, IDragHandler, IEndDragHandler
{
    private RectTransform rectTransform;
    private Canvas canvas;
    private CanvasGroup canvasGroup;
    private Transform previusParent;
    public Item item;

    private void Start() {
        rectTransform = GetComponent<RectTransform>();
        canvas = GetComponentInParent<Canvas>();
        canvasGroup = GetComponent<CanvasGroup>();
    }

    //Metodo que se ejecuta cuando estamos empezando a arrastar
    public void OnBeginDrag(PointerEventData eventData)
    {
       Debug.Log("OnBeginDrag");
       //Guardamos el slot en el que inicialmente se encuentra el item
       previusParent = transform.parent;
       //Cambiamos el padre a el canvas
       transform.parent = canvas.transform;
        //Desactivamos el bloqueo del raycast
       canvasGroup.blocksRaycasts = false;
       //Cambiamos el alpha a 0.6
       canvasGroup.alpha = 0.6f;
    }

    //Funcion que se ejecuta mientras estamos arrastarando
    public void OnDrag(PointerEventData eventData)
    {
        Debug.Log("OnDrag");
        //Sumamos la posicion del item segun el movimiento del raton dividido entre la escala de canvas
        rectTransform.anchoredPosition += eventData.delta/canvas.scaleFactor;
    }
    //Metodo que se ejecuta cuando finaliza el arrastre
    public void OnEndDrag(PointerEventData eventData)
    {
        Debug.Log("OnEndDrag");
        //Si el padre del item es canvas
        if(transform.parent.Equals(canvas.transform)){
            //Cambiamos el padre al slot de inicio
            transform.parent = previusParent;
            //Ponemos la posicion local a 0
            rectTransform.localPosition = Vector3.zero;
        }
        //Activamos el bloque del raycast
        canvasGroup.blocksRaycasts = true;
        //Ponemos el alpha a 1
        canvasGroup.alpha = 1f;
    }
}
