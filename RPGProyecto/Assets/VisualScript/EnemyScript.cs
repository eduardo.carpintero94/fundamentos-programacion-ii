using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class EnemyScript : MonoBehaviour
{
    [SerializeField] private LayerMask playerLayer;

    public bool CheckViewPlayer(float viewDistance,float angleMin, float angleMax)
    {
        Transform playerTr = GameManager.instance.player.transform;

        float distance = Vector3.Distance(transform.position, playerTr.position);
        if (distance <= viewDistance)
        {
            Vector3 direction = (playerTr.position - transform.position);
            float angle = Vector3.Angle(transform.forward,direction);
            
            if(angle >= angleMin && angle <= angleMax)
            {
                RaycastHit hit;
                Vector3 posRay = transform.position;
                posRay.y += 0.5f;
                if (Physics.Raycast(posRay,direction,out hit,Mathf.Infinity,playerLayer))
                {
                    Debug.Log(hit.collider.name);
                    return true;
                }
            }
        }

        return false;
    }
}
