using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ModifyMesh : MonoBehaviour
{
    //Mesh a generar
    private Mesh mesh;
    //Array de vertices
    private Vector3[] vertices;
    //Filtro de mesh al que pasaremos la mesh
    private MeshFilter meshFilter;
    //Transform del jugador
    private Transform trPlayer;

    // Start is called before the first frame update
    void Start()
    {
        //Recogemos el meshFilter
        meshFilter = GetComponent<MeshFilter>();

        //Recogemos la mesh de meshFilter
        mesh = meshFilter.mesh;
        //Recogemos los vertices del mesh
        vertices = mesh.vertices;
        //Buscamos el transform del jugador
        trPlayer = GameObject.FindGameObjectWithTag("Player").transform;
    }

    // Update is called once per frame
    void Update()
    {
        //Recorremos el array
        for(int i=0;i<vertices.Length;i++){
            //calculamos las distancia entre el vertice y el jugador
            //Si esta a menos 1.1 de distancia
            if(Vector3.Distance(vertices[i],trPlayer.position) <= 1.1f){
                //vajamos el vertices
                vertices[i].y -=2;
            }
        }

        //Pasamos los vertices a la mesh
        mesh.vertices = vertices;
        //Recalculamos los Bounds
        mesh.RecalculateBounds();
        //Recalculamos los normales
        mesh.RecalculateNormals();
        //Pasamos la mesh al meshFilter
        meshFilter.mesh = mesh;
    }
}
