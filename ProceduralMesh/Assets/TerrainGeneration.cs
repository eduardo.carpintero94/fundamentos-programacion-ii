using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class TerrainGeneration : MonoBehaviour
{
    [SerializeField]private int width = 500;
    [SerializeField]private float depth = 60;
    [SerializeField]private int height = 500;
    [SerializeField] private float scale = 20;
    [SerializeField]private int grassDensity = 100;
    [Range(8,128)]
    [SerializeField]private int grassPerPatch = 16;

    private float xOffset = 0;
    private float yOffset = 0;
    

    void Start()
    {
        //Offset aleatorio
        xOffset = UnityEngine.Random.Range(0,999);
        yOffset = UnityEngine.Random.Range(0,999);
        //Recogemos el terrain
        Terrain terrain = GetComponent<Terrain>();
        //Creamos el terrain data a partir del terrainData que ya tenemos
        terrain.terrainData = GenerateTerrain(terrain.terrainData);
        //Generamos los arboles
        GenerateTree(terrain);

        GenerateDetails(terrain);
    }

    //Funcion de generacion del terreno
    private TerrainData GenerateTerrain(TerrainData terrainData)
    {
        //Cambiamos la resolucion del heighmap
        terrainData.heightmapResolution = height+1;
        //Cambiamos al size
        terrainData.size = new Vector3(width,depth,height);
        //Creamos las alturas
        terrainData.SetHeights(0,0,GenerateHeights());
        //retornamos terrainData
        return terrainData;
    }

    //Generamos las alturas
    private float[,] GenerateHeights()
    {
        //Array bidimensional de alturas
        //El tamanio es segun el ancho y el alto del array
       float[,] heights = new float[width,height];
       for(int x=0;x<width;x++){
           for(int y=0;y<height;y++){
               //Calculamos la altura para esa coordenada segun el ancho y el alto
               heights[x,y] = CalculateHeight(x,y);
           }
       }
        //Retornamos el array
       return heights;
    }

    //Calculamos la altura
    private float CalculateHeight(int x, int y)
    {
        //Calculamos la coordenada X segun el ancho, la escala y el offset
        float xCoord = (float)x/width*scale+xOffset;
        //Calculamos la coordenada Y segun el alto, la escala y el offset
        float yCoord = (float)y/height*scale+yOffset;
        //Retornamos el PerlinNoise
        return Mathf.PerlinNoise(xCoord,yCoord);
    }

    //Metodo que genera arboles
    private void GenerateTree(Terrain terrain)
    {
        //Vaciamos los arboles que haya en el terrain
        terrain.terrainData.treeInstances = new TreeInstance[0];
        //Bucle del ancho del terrain
        for(int x=0;x<width;x++){
            //Bucle del alto del terrain
            for(int z=0;z<height;z++){
                //Generamos un numero aleatorio
                int r = UnityEngine.Random.Range(0,500);
                //Si es igual a 0
                if(r.Equals(0)){
                    //Creamos una instancia de arbol
                    TreeInstance treeTemp = new TreeInstance();
                    //Calculamos su posicion del terrain
                    //en los ejes es la division de la coordenada entre el tamanio de ese eje del terrain
                    treeTemp.position = new Vector3(x/terrain.terrainData.size.x,0,z/terrain.terrainData.size.z);
                    ///Selecionamos un arbol al azar
                    treeTemp.prototypeIndex = UnityEngine.Random.Range(0,terrain.terrainData.treePrototypes.Length);
                    //Ancho del arbol
                    treeTemp.widthScale = 1;
                    //Alto del arbol
                    treeTemp.heightScale = 1;
                    //Color del arbol, blanco hace que coja el color de la textura
                    treeTemp.color = Color.white;
                    //Color del ligmap
                    treeTemp.lightmapColor = Color.white;
                    //Aniadimos el arbol al terrain
                    terrain.AddTreeInstance(treeTemp);
                }
            }
        }
        //Mostramos el numero de arboles en consola
        Debug.Log(terrain.terrainData.treeInstanceCount);
    }

    //Metodo para generar detalles en el terreno
    private void GenerateDetails(Terrain terrain)
    {
        //Cambiamos la resolucion de los detalles del terrain
        terrain.terrainData.SetDetailResolution(grassDensity,grassPerPatch);
        //Array bidiensional con los detalles
        //Segun el alto y ancho
        int[,] details = new int[width,height];
        for(int x =0;x<width;x++){
            for(int y=0;y<height;y++){
                //Recogemos la altura terrain
                float height = terrain.terrainData.GetHeight(x,y);
                //La convertimos a entero y lo guardamos en el array del detalles
                details[x,y] = (int)height;
            }
        }
        
        //Recogemos de manera aleatoria que detalle vamos a instanciar
        int layerDetail = UnityEngine.Random.Range(0,terrain.terrainData.detailPrototypes.Length);
        //Se lo pasamos al terrain data
        terrain.terrainData.SetDetailLayer(0,0,layerDetail,details);
    }
}
