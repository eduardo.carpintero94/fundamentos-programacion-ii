using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEditor;

public class ProceduralMesh : MonoBehaviour
{
    //Ancho
    [SerializeField] private int xSize = 20;
    //Alto
    [SerializeField] private int zSize = 20;
    //Multiplicador del perlinNoise
    [Range(0.1f,0.9f)]
    [SerializeField] private float perlinNoiseMultipler = 0.3f;
    //Escala del perlinNoise
    [SerializeField] private float perlinNoiseScaler = 10;

    //Mesh que vamos a crear
    private Mesh mesh;
    //Array de vertices
    private Vector3[] vertices;
    //Array de triangulos
    private int[] triangles;
    //Incremento del eje Y
    private float yIncrement = 0;

    private void Start() {
        //Generamos la mesh
        GenerateMesh();
        //Actualziamos la mesh
        UpdateMesh();
        
    }
    private void Update() {
        if(Input.GetKeyDown(KeyCode.S)){
            //Creamos un archivo .assetcon nuestra maya
            AssetDatabase.CreateAsset(mesh,"Assets/myMesh.asset");
        }
    }

    private void GenerateMesh()
    {
        //Creamos una nueva mesh
        mesh = new Mesh();

        //Calculamos el numero de vertices
        int numVertices = (xSize+1)*(zSize+1);
        //Pasamos el numero de vertices al tamanio del array
        vertices = new Vector3[numVertices];
        //Variable que se incrementara para recorrer el array de los vertices
        int i = 0;
        //Recorremos el largo
        for(int z = 0;z<=zSize;z++){
            //Recorremos el ancho
            for(int x=0;x<=xSize;x++){
                /*float y = 0;
                if(i%2==0){
                     y = yIncrement;
                    if(Input.GetKey(KeyCode.M)){
                        yIncrement +=0.00001f;
                    }
                    
                }*/
                //Calculamos el eje Y con el perlinNoise segun el eje X y el eje Z
                float y = Mathf.PerlinNoise(x*perlinNoiseMultipler,z*perlinNoiseMultipler)*perlinNoiseScaler;
                Debug.Log(y);
                //Le pasamos los ejes X,Y,Z al vertice
                vertices[i] = new Vector3(x,y,z);
                //Incrementamos la variable
                i++;
            }
        }

        //Calculamos el numero de triangulos
        int numTriangles = (xSize*zSize*6);
        //Se lo pasamos al tamanio del array
        triangles = new int[numTriangles];

        //Vertice en el que estamos,esta variable se ira incrementando
        int vert = 0;
        //Triangulos en el que estamos,esta variable se ira incrementando
        int tris = 0;
        //Recorremeos el alto
        for(int z=0;z<zSize;z++){
            //Recorremos el ancho
            for(int x=0;x<xSize;x++){
                //Decimos que vertices son los distintos triangulos de la maya
                triangles[tris+0] = vert+0;
                triangles[tris+1] = vert+xSize+1;
                triangles[tris+2] = vert+1;

                triangles[tris+3] = vert+1;
                triangles[tris+4] = vert+xSize + 1;
                triangles[tris+5] = vert+xSize + 2;
                //Incrmeentamos las variables
                vert++;
                tris+=6;
            }
            //Incrementamos la variable
            vert++;
        }
    }

    private void UpdateMesh()
    {
        //Limpiamos la maya
        mesh.Clear();
        //Le pasamos el numero de vertices y triangulos
        mesh.vertices = vertices;
        mesh.triangles = triangles;
        //Recalculamos las normales
        mesh.RecalculateNormals();
        //Pasamos la maya al meshFIlter
        GetComponent<MeshFilter>().mesh = mesh;
        //Pasamos la maya al MeshCOllider
        GetComponent<MeshCollider>().sharedMesh = mesh;
    }

/*#if UNITY_EDITOR
    private void OnDrawGizmos() {
        Gizmos.color = Color.red;

        if(vertices== null){
            return;
        }

        for(int i = 0;i<vertices.Length;i++){
            Gizmos.DrawSphere(vertices[i],0.2f);
        }
    }
#endif*/
}
