using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class MoveParticle : MonoBehaviour
{
    private ParticleSystem particleSystem;
    private ParticleSystem.Particle[] particles;

    [SerializeField] private Transform target;
    [SerializeField] private float speed = 10f;
    [SerializeField] private Color32 colorParticle;

    // Start is called before the first frame update
    void Start()
    {
        particleSystem = GetComponent<ParticleSystem>();
    }

    // Update is called once per frame
    void Update()
    {
        particles = new ParticleSystem.Particle[particleSystem.particleCount];

        particleSystem.GetParticles(particles,particles.Length);

        for(int i=0;i<particles.Length;i++){

            /*particles[i].position = Vector3.MoveTowards(particles[i].position,target.position,
                speed*Time.deltaTime);
            particles[i].position = Vector3.Lerp(particles[i].position,target.position,speed*Time.deltaTime);
            if(i%2==0){
                particles[i].startColor = colorParticle;
            }*/
            
            // speed = (StartLife-remainingLife)*distancia
            // position += speed*direction

            /*float startLife = particleSystem.main.startLifetime.constant;
            float remainingLife = particles[i].remainingLifetime;
            float distance = Vector3.Distance(particles[i].position,target.position);

            float speedParticles = (startLife - remainingLife) * distance;
            Vector3 direction = (target.position-particles[i].position).normalized;
            
            particles[i].position += direction*speedParticles;*/

            float startSpeed = particleSystem.main.startSpeed.constant;
            float lifePartcile = particles[i].remainingLifetime;
            float speedParticles = (startSpeed/lifePartcile)+startSpeed;

            Vector3 direction = (target.position-particles[i].position).normalized;

            particles[i].position += speedParticles*direction*Time.deltaTime;
        }

        particleSystem.SetParticles(particles,particles.Length);
    }
}
