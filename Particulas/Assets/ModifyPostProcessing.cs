using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Rendering.PostProcessing;

public class ModifyPostProcessing : MonoBehaviour
{
    private PostProcessVolume postProcessVolume;
    private bool start = false;
    private Bloom bloom;

    // Start is called before the first frame update
    void Start()
    {
        postProcessVolume = GetComponent<PostProcessVolume>();
        postProcessVolume.profile.TryGetSettings(out bloom);
    }

    // Update is called once per frame
    void Update()
    {
        if(Input.GetKeyDown(KeyCode.B)){
            //start = true;
            bloom.active = !bloom.active;
        }

        if(start){
            if(bloom.intensity.value < 1000){
                bloom.intensity.value++;
            }else{
                start = false;
            }
        }
    }
}
