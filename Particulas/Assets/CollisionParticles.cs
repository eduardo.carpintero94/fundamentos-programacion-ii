using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CollisionParticles : MonoBehaviour
{
    List<ParticleCollisionEvent> particleCollisions = new List<ParticleCollisionEvent>();
    ParticleSystem particleSystem;
    private void Start() {
        particleSystem = GetComponent<ParticleSystem>();
    }

    private void OnParticleCollision(GameObject other) {
        particleSystem.GetCollisionEvents(other,particleCollisions);

        Rigidbody rb = other.GetComponent<Rigidbody>();
        for(int i=0;i<particleCollisions.Count;i++){
            rb.AddForce(particleCollisions[i].velocity*10);
        }
    }
}
