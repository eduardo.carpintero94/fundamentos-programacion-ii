using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ParticleControll : MonoBehaviour
{
    private ParticleSystem particleSystem;

    // Start is called before the first frame update
    void Start()
    {
        particleSystem = GetComponent<ParticleSystem>();
    }

    // Update is called once per frame
    void Update()
    {
        if(Input.GetKeyDown(KeyCode.P)){
            //Si la particula se esta ejecutando
            if(particleSystem.isPlaying){
                //Se pausa
                //particleSystem.Pause();
                //Se para
                particleSystem.Stop();
            //Si no
            }else{
                //Ejecutamos la particula
                particleSystem.Play();
            }
        }
    }
}
