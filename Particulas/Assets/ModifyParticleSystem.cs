using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ModifyParticleSystem : MonoBehaviour
{
    private ParticleSystem particleSystem;

    // Start is called before the first frame update
    void Start()
    {
        particleSystem = GetComponent<ParticleSystem>();

        StartCoroutine(AmountEmission());
    }

    IEnumerator AmountEmission(){
        yield return new WaitForSeconds(5f);
        var emission = particleSystem.emission;
        emission.rateOverTime = 100;

        var main = particleSystem.main;
        main.startColor = Color.red;

        StartCoroutine(DismountEmission());
    }

    IEnumerator DismountEmission(){
        yield return new WaitForSeconds(5f);

        var emission = particleSystem.emission;
        emission.rateOverTime = 10;

        var main = particleSystem.main;
        main.startColor = Color.white;

        StartCoroutine(AmountEmission());
    }
}
